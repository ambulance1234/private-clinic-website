-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: private_clinic_inno_db
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'maccereal@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357140',20,'Cereal','Mac',NULL,_binary '',1,1,1,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(2,'maccereal10@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357141',20,'Cereal','Mc\'Donald',NULL,_binary '',1,1,1,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(3,'maccereal11@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357142',20,'Cereal','Mac Colony',NULL,_binary '',1,1,1,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(4,'maccereal12@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357143',20,'Cereal','Mac Compac',NULL,_binary '',1,2,3,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(5,'maccereal13@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357144',20,'Cereal','Jimmy',NULL,_binary '',1,2,3,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(6,'maccereal14@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357145',20,'Lil','Gray',NULL,_binary '',1,2,4,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(7,'maccereal15@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357146',20,'Schiskal','Lawrence',NULL,_binary '',2,3,5,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(8,'maccereal16@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357147',20,'Schiskal','Anna',NULL,_binary '',2,3,5,'2022-04-05 00:00:00','2022-04-07 23:42:34'),(9,'maccereal17@milosclinic.com','$2a$10$SclFpKPyyf./j0nxUzr/1.0oBIGVGLKXuzVzNz.EZ5G5XbiU7A4eq','(+84)963357148',20,'Schiskal','Bella',NULL,_binary '',2,3,5,'2022-04-05 00:00:00','2022-04-07 23:42:34');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `detail_drugs_prescriptions`
--

LOCK TABLES `detail_drugs_prescriptions` WRITE;
/*!40000 ALTER TABLE `detail_drugs_prescriptions` DISABLE KEYS */;
INSERT INTO `detail_drugs_prescriptions` VALUES (1,4,1,1,1,0.59,'10',1,2,5,7),(1,4,1,2,2,1300,'2',3,1,1,1),(1,4,1,3,3,2.7,'10',2,2,5,2),(2,4,1,1,4,0.6,'10',1,2,1,7),(2,4,1,2,5,52,'10',3,2,1,7),(2,4,1,3,6,200,'10',2,2,1,7);
/*!40000 ALTER TABLE `detail_drugs_prescriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `drugs`
--

LOCK TABLES `drugs` WRITE;
/*!40000 ALTER TABLE `drugs` DISABLE KEYS */;
INSERT INTO `drugs` VALUES (1,'VD-23889-15','Casoran','Công ty cổ phần công nghệ cao Traphaco','Việt Nam',0.59,1),(2,'VN-17674-14','Docetaxel 20 mg','Teva Pharmaceutical Works Private Limited Company','Hungary',1300,3),(3,'VD-24071-16','Hà thủ ô','Công ty cổ phần công nghệ cao Traphaco','Việt Nam',2.7,2),(4,'VN-17635-14','1-AL Levocetirizine','FDC Limited','India',0.6,1),(5,'VN-17818-14','1-AL Levocetirizine Dihydrochloride','FDC Limited','India',52,3),(6,'VN-19115-15','20% Fat Emulsion Injection Soybean oil','Guangdong Otsuka Pharmaceutical Co. Ltd.','Trung Quốc',200,2),(7,'VD-22915-15','3B-Medi','Công ty cổ phần dược phẩm Me Di Sun','Việt Nam',1.25,1),(8,'VD-26870-17','3B-Medi tab','Công ty cổ phần dược phẩm Me Di Sun','Việt Nam',1.1,1),(9,'VD-26140-17','3BTP','Công ty cổ phần dược phẩm Hà Tây','Việt Nam',1.25,1),(10,'VD-16258-12','3Bpluzs F','Công ty cổ phần Dược phẩm Phương Đông','Việt Nam',1.4,1),(11,'VD-17429-12','3Bvit ando','Công ty cổ phần dược phẩm Hà Tây','Việt Nam',1.4,1),(12,'VN2-52-13','4-Epeedo-50','Naprod Life Sciences Pvt. Ltd.','Ấn Độ',573,3),(13,'VN-18586-15','4.2% w/v Sodium Bicarbonate','B.Braun Melsungen AG','Đức',95,5),(14,'VN-16866-13','5% Dextrose 500ml inj Infusion','Dai Han Pharm. Co. Ltd.','Korea',12,5),(15,'VN-17422-13','5-Fluorouracil Ebewe','Ebewe Pharma Ges.m.b.H.Nfg.KG','Áo',112.35,3),(16,'VN2-112-13','8 Horas Eszopiclone','Laboratorio Elea S.A.C.I.F.yA','Argentina',4,1),(17,'VN2-113-13','8 Horas Eszopiclone','Laboratorio Elea S.A.C.I.F.yA','Argentina',4,1),(18,'VN-21186-18','9PM Latanoprost','Cipla Ltd','India',150,3),(19,'VD-18416-13','A9 - Cerebrazel','Công ty cổ phần dược TW Mediplantex','Việt Nam',0.9,1),(20,'VD-20748-14','ABAB 500 mg','Công ty cổ phần dược phẩm IMEXPHARM','Việt Nam',0.286,1),(21,'VD-20749-14','ABAB 500 mg','Công ty cổ phần dược phẩm IMEXPHARM','Việt Nam',0.286,1),(22,'VN-19978-16','ACC 200 mg','Lindopharm GmbH','Đức',2.677,6),(23,'VD-25107-16','ACM Control 1','Công ty cổ phần Dược phẩm 3/2','Việt Nam',2.4,1),(24,'VD-25594-16','ACM Control 4','Công ty cổ phần Dược phẩm 3/2','Việt Nam',3,1),(25,'VD-20653-14','AG-Ome Omeprazol','Công ty cổ phần dược phẩm Agimexpharm','Việt Nam',1.35,1),(26,'VD-26141-17','AN KHỚP VƯƠNG','Công ty cổ phần dược phẩm Hà Tây','Việt Nam',5,1),(27,'VD-17911-12','ATP','Công ty cổ phần dược phẩm Hà Tây','Việt Nam',0.41,1),(28,'VD-27208-17','ATP','Công ty cổ phần dược TW Mediplantex','Việt Nam',0.95,1),(29,'VN-18861-15','Aarmol 100ml','Shree Krishnakeshav Laboratories Limited','Ấn Độ',21,5),(30,'VN2-643-17','Abacavir Tablets USP 300mg','Mylan Laboratories Limited','Ấn Độ',11,1),(31,'VN3-27-18','Abamune-L Baby','Cipla Ltd.','Ấn Độ',9,1),(32,'VN-20441-17','Abbsin 200','OU Vitale-XD (nơi sản xuất Vitale Pringi)','Estonia',6.6,1),(33,'VN-20442-17','Abbsin 600','OU Vitale-XD (nơi sản xuất Vitale Pringi)','Estonia',14.8,1),(34,'VN-17095-13','Abernil 50 mg','Medochemie Ltd.','Cyprus',24,1),(35,'VN-16372-13','Abhigrel 75','Medibios Laboratories Pvt. Ltd.','Ấn Độ',2.1,1),(36,'VN3-82-18','Abilify tablets 15 mg','Korea Otsuka Pharmaceutical Co. Ltd.','Hàn Quốc',2,1),(37,'VN2-53-13','Abingem 200','Naprod Life Sciences Pvt. Ltd.','India',547.5,3),(38,'VN2-392-15','Abingem-1gm','Naprod Life Sciences Pvt. Ltd.','Ấn Độ',600,3),(39,'VN-16899-13','Abitrax','Laboratorio Farmaceutico C.T.s.r.l.','Ý',52,3),(40,'NC49-H12-15','Abivina','Viện Dược liệu','Việt Nam',3.085,1),(41,'VD-25057-16','Abochlorphe','Chi nhánh công ty TNHH SX-TM dược phẩm Thành Nam','Việt Nam',0.106,1),(42,'VD-18035-12','Abrocto','Công ty cổ phần dược-vật tư y tế Thanh Hoá','Việt Nam',1.45,6),(43,'VN-21345-18','Acabrose Tablets 50mg','Standard Chem. & Pharm. Co. Ltd.','Đài Loan',2.25,1),(44,'VD-24153-16','Acarfar','Công ty cổ phần dược phẩm dược liệu Pharmedic','Việt Nam',2.1,1),(45,'VN-20830-17','Acc Pluzz 200','Hermes Arzneimittel GmbH','Đức',6.7,1),(46,'VN-20831-17','Acc Pluzz 600','Hermes Arzneimittel GmbH','Đức',12,1),(47,'VD-17887-12','Ace kid 150','Công ty cổ phần dược phẩm Bidiphar 1','Việt Nam',1.26,6),(48,'VD-18248-13','Ace kid 325','Công ty cổ phần dược phẩm Bidiphar 1','Việt Nam',2.94,6),(49,'VD-16364-12','Acebis - 1,5g','Công ty cổ phần tập đoàn Merap','Việt Nam',56,3),(50,'VD-16365-12','Acebis - 1g','Công ty cổ phần tập đoàn Merap','Việt Nam',56,3),(51,'VD-16366-12','Acebis - 2,25g','Công ty cổ phần tập đoàn Merap','Việt Nam',84,3),(52,'VD-20124-13','Aceclofenac Stada 100 mg','Công ty TNHH LD Stada-Việt Nam','Việt Nam',0.701,1),(53,'VD-21705-14','Aceclofenac T/H','Công ty cổ phần dược-vật tư y tế Thanh Hoá','Việt Nam',4.5,1),(54,'VN-20696-17','Aceclonac','Rafarm S.A.','Hy Lạp',5.9,1),(55,'VN-21262-18','Acectum','Karnataka Antibiotics & Pharmaceuticals Limited','Ấn Độ',160,3),(56,'VD-25112-16','Acecyst','Chi nhánh công ty cổ phần dược phẩm Agimexpharm- Nhà máy sản xuất dược phẩm Agimexpharm','Việt Nam',0.85,1),(57,'VD-26076-17','Acedolflu','Công ty cổ phần dược phẩm 2/9 TP HCM','Việt Nam',2,1),(58,'VD-23527-15','Acefalgan 150','Công ty cổ phần Dược phẩm Euvipharm - Thành viên tập đoàn Valeant','Việt Nam',1.425,6),(59,'VD-25673-16','Acefalgan 250','Công ty cổ phần Dược phẩm Euvipharm - Thành viên tập đoàn Valeant','Việt Nam',1.8,6),(60,'VD-23528-15','Acefalgan 500','Công ty cổ phần Dược phẩm Euvipharm - Thành viên tập đoàn Valeant','Việt Nam',2.1,1),(61,'VD-26134-17','Acefalgan 500','Công ty cổ phần Dược phẩm Euvipharm - Thành viên tập đoàn Valeant','Việt Nam',0.918,1),(62,'VD-26135-17','Acefalgan Codein','Công ty cổ phần Dược phẩm Euvipharm - Thành viên tập đoàn Valeant','Việt Nam',2.63,1),(63,'VD-15582-11','Acehasan 100','Công ty TNHH Ha san - Dermapharm','Việt Nam',1.02,6),(64,'VD-15583-11','Acehasan 200','Công ty TNHH Ha san - Dermapharm','Việt Nam',1.14,6),(65,'VD-19179-13','Acehasan 200','Công ty TNHH Ha san - Dermapharm','Việt Nam',0.465,1),(66,'VD-27875-17','Acemetin','Công ty cổ phần dược phẩm Hà Tây','Việt Nam',1.65,6),(67,'VD-24693-16','Acemol fort','Công ty cổ phần dược phẩm 2/9 TP HCM','Việt Nam',0.56,1),(68,'VD-18156-12','Acemuc','Công ty TNHH Sanofi-Aventis Việt Nam','Việt Nam',2.32,1),(69,'VD-17405-12','Acenac 100','Công ty cổ phần dược phẩm Cửu Long','Việt Nam',0.54,1),(70,'VD-20678-14','Acepron 250 mg','Công ty cổ phần dược phẩm Cửu Long','Việt Nam',0.75,6),(71,'VD-20679-14','Acepron 325 mg','Công ty cổ phần dược phẩm Cửu Long','Việt Nam',1.8,6),(72,'VD-16514-12','Acepron 325 mg','Công ty cổ phần dược phẩm Cửu Long','Việt Nam',0.195,1),(73,'VD-20680-14','Acepron 500 mg','Công ty cổ phần dược phẩm Cửu Long','Việt Nam',0.3,1),(74,'VD-22822-15','Acepron 650','Công ty cổ phần dược phẩm Cửu Long','Việt Nam',0.435,1),(75,'VD-20681-14','Acepron Codein','Công ty cổ phần dược phẩm Cửu Long','Việt Nam',1.365,1),(76,'VD-22437-15','Acetab 325','Công ty cổ phần dược phẩm Agimexpharm','Việt Nam',0.27,1),(77,'VD-26090-17','Acetab 650','Chi nhánh công ty cổ phần dược phẩm Agimexpharm- Nhà máy sản xuất dược phẩm Agimexpharm','Việt Nam',0.7,1),(78,'VD-17975-12','Acetalvic codein 30','Công ty cổ phần dược phẩm trung ương VIDIPHA','Việt Nam',1.1,1),(79,'VD-24239-16','Acetaphen 500','Công ty cổ phần dược phẩm OPV','Việt Nam',0.5,1),(80,'VD-20935-14','Acethepharm','Công ty cổ phần dược-vật tư y tế Thanh Hoá','Việt Nam',1.2,6),(81,'VD-20936-14','Acethepharm','Công ty cổ phần dược-vật tư y tế Thanh Hoá','Việt Nam',1.6,6),(82,'VD-20043-13','Acetydona 200 mg','Công ty cổ phần sản xuất - thương mại Dược phẩm Đông Nam','Việt Nam',0.55,1),(83,'VD-17430-12','Acetyl Cystein','Công ty cổ phần dược phẩm Hà Tây','Việt Nam',1.5,6),(84,'VD-23150-15','Acetyl Max','Công ty cổ phần dược vật tư y tế Thanh Hoá','Việt Nam',0.7,1),(85,'VD-17864-12','Acetylcystein','Công ty cổ phần dược phẩm 2/9 - Nadyphar','Việt Nam',2.1,6),(86,'VD-20260-13','Acetylcystein','Công ty cổ phần dược phẩm TV. Pharm','Việt Nam',0.7,1),(87,'VD-27595-17','Acetylcystein','Chi nhánh công ty TNHH SX-TM dược phẩm Thành Nam','Việt Nam',0.57,1),(88,'VD-18919-13','Acetylcystein','Công ty cổ phần Dược phẩm 3/2','Việt Nam',0.7,1),(89,'VD-22770-15','Acetylcystein 200 mg','Công ty cổ phần Dược Minh Hải','Việt Nam',0.45,1),(90,'VD-17186-12','Acetylcystein 200 mg','Công ty CP liên doanh dược phẩm Medipharco Tenamyd BR s.r.l','Việt Nam',2.2,1),(91,'VD-23445-15','Acetylcystein 200 mg','Công ty cổ phần dược Đồng Nai','Việt Nam',0.75,1),(92,'VD-21910-14','Acetylcystein 200 mg','Chi nhánh công ty cổ phần dược phẩm trung ương Vidipha tại Bình Dương','Việt Nam',0.407,1),(93,'VD-20019-13','Acetylcysteine 200 mg','Công ty cổ phần hoá-dược phẩm Mekophar','Việt Nam',0.8,1),(94,'VD-20188-13','Aciclovir','Công ty cổ phần dược phẩm dược liệu Pharmedic','Việt Nam',2.23,1),(95,'VD-16803-12','Aciclovir 200 mg','Công ty cổ phần Dược Minh Hải','Việt Nam',0.45,1),(96,'VD-17856-12','Aciclovir 400 mg','Công ty cổ phần Dược Minh Hải','Việt Nam',0.68,1),(97,'VD-18434-13','Aciclovir 5%','Công ty cổ phần dược vật tư y tế Hải Dương','Việt Nam',10,7),(98,'VN-17013-13','Aciclovir Tablets BP','Brawn Laboratories Ltd','Ấn Độ',0.93,1),(99,'VD-16826-12','Acid Folic 5 mg','Công ty cổ phần Dược phẩm 3/2','Việt Nam',0.252,1),(100,'VD-19107-13','Acid folic MKP','Công ty cổ phần hoá-dược phẩm Mekophar','Việt Nam',0.54,1);
/*!40000 ALTER TABLE `drugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `genders`
--

LOCK TABLES `genders` WRITE;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT INTO `genders` VALUES (1,'Male'),(2,'Female');
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `medical_examinations`
--

LOCK TABLES `medical_examinations` WRITE;
/*!40000 ALTER TABLE `medical_examinations` DISABLE KEYS */;
INSERT INTO `medical_examinations` VALUES (1,4,1,'2022-03-20 09:00:00','Đau bụng','2022-03-06 00:00:00','2022-03-06 00:00:00'),(1,4,2,'2022-03-20 10:00:00','Đau bụng','2022-03-06 00:00:00','2022-03-06 00:00:00'),(1,5,1,'2022-03-20 13:00:00','Đau đầu','2022-03-06 00:00:00','2022-03-06 00:00:00'),(1,5,2,'2022-03-20 12:00:00','Đau đầu','2022-03-06 00:00:00','2022-03-06 00:00:00'),(1,6,1,'2022-03-20 15:00:00','Đau lưng','2022-03-06 00:00:00','2022-03-06 00:00:00'),(1,6,2,'2022-03-20 13:00:00','Đau lưng','2022-03-06 00:00:00','2022-03-06 00:00:00'),(2,4,2,'2022-03-25 08:00:00','Tái khám bụng','2022-03-20 00:00:00','2022-03-20 00:00:00'),(2,5,1,'2022-03-26 13:00:00','Tái khám đầu','2022-03-20 00:00:00','2022-03-20 00:00:00'),(2,5,2,'2022-03-26 12:00:00','Tái khám đầu','2022-03-20 00:00:00','2022-03-20 00:00:00'),(2,6,2,'2022-03-26 14:00:00','Tái khám lưng','2022-03-06 00:00:00','2022-03-06 00:00:00');
/*!40000 ALTER TABLE `medical_examinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `parts_of_day`
--

LOCK TABLES `parts_of_day` WRITE;
/*!40000 ALTER TABLE `parts_of_day` DISABLE KEYS */;
INSERT INTO `parts_of_day` VALUES (1,'morning'),(2,'noon'),(3,'afternoon'),(4,'evening'),(5,'morning - noon'),(6,'morning - afternoon'),(7,'morning - evening'),(8,'noon - afternoon'),(9,'noon - evening'),(10,'afternoon - evening'),(11,'mor-noon-eve'),(12,'mor-noon-aft');
/*!40000 ALTER TABLE `parts_of_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,'Miranda','Fischer','07920004300',150,50,20,'15th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(2,'Rowan','Schneider','07920004301',170,60,20,'15th Wall Street NY',2,'2022-04-06 07:00:00','2022-04-13 07:48:00'),(3,'Doris','Dubois','07920004302',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(4,'Miranda','Meyer','07920004303',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(5,'Drusilla','Koch','07920004304',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(6,'Eirian','Bauer','07920004305',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(7,'Kaylin','Schmidt','07920004306',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(8,'Rowan','Becker','07920004307',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(9,'Anna','Wagner','07920004308',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(10,'Kiana','Weber','07920004309',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(11,'Mizuki','Sato','07920004310',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(12,'Izumo','Shakamoto','07920004311',170,70,20,'5th Wall Street NY',2,'2022-04-06 00:00:00','2022-04-06 00:00:00'),(13,'Hung','Phung','07920004312',169,69,21,'55/11/2, đường 15, TP. Thủ Đức',1,'2022-04-11 20:12:37','2022-04-11 20:12:37');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `prescriptions`
--

LOCK TABLES `prescriptions` WRITE;
/*!40000 ALTER TABLE `prescriptions` DISABLE KEYS */;
INSERT INTO `prescriptions` VALUES (1,4,1,'Phình động mạch chủ','Thiếu máu lên não, da tái nhợt, người mệt mỏi','Uống thuốc','2021-04-15 07:15:00','2021-04-15 07:15:00','2021-04-15 07:15:00'),(1,5,1,'Răng miệng bình thường','Bình thường','Không','2021-04-15 08:15:00','2021-04-15 08:15:00','2021-04-15 08:15:00'),(1,6,2,'Viêm bàng quang','Tiểu buốt','Uống thuốc','2021-04-15 07:15:00','2021-04-15 07:15:00','2021-04-15 07:15:00'),(1,7,2,'Dạ dày bình thường','Thiếu máu lên não, da tái nhợt, người mệt mỏi','Uống thuốc','2022-04-15 07:15:00','2022-04-15 07:15:00','2022-04-15 07:15:00'),(2,4,1,'Động kinh','Thiếu máu lên não, da tái nhợt, người mệt mỏi','Uống thuốc','2021-04-16 07:15:00','2021-04-16 07:15:00','2021-04-16 07:15:00'),(2,5,1,'Mũi chấn thương độ 2','Khó thở, bị thương ở mũi','Băng bó bằng thuốc đỏ, không kê đơn','2022-04-15 09:15:00','2022-04-15 09:15:00','2022-04-15 09:15:00'),(3,4,1,'Nhồi van tim','Thiếu máu lên não, da tái nhợt, người mệt mỏi','Uống thuốc','2021-04-17 07:15:00','2021-04-17 07:15:00','2021-04-17 07:15:00'),(4,4,1,'Cao huyết áp','Thiếu máu lên não, da tái nhợt, người mệt mỏi','Uống thuốc','2022-04-15 07:15:00','2022-04-15 07:15:00','2022-04-15 07:15:00');
/*!40000 ALTER TABLE `prescriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ADMIN'),(2,'DOCTOR'),(3,'NURSE');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'DBA.'),(2,'SA.'),(3,'PROF.'),(4,'DR.'),(5,'NURS.');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (1,'pill - viên'),(2,'bag - túi'),(3,'potion - lọ'),(4,'tube - ống'),(5,'bottle - chai'),(6,'pack - gói'),(7,'type - tuýp');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-16  0:02:03
