/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "drugs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Drugs.findAll", query = "SELECT d FROM Drugs d"),
    @NamedQuery(name = "Drugs.findByDrugId", query = "SELECT d FROM Drugs d WHERE d.drugId = :drugId"),
    @NamedQuery(name = "Drugs.findByRegistedCode", query = "SELECT d FROM Drugs d WHERE d.registedCode = :registedCode"),
    @NamedQuery(name = "Drugs.findByDrugName", query = "SELECT d FROM Drugs d WHERE d.drugName = :drugName"),
    @NamedQuery(name = "Drugs.findByCompany", query = "SELECT d FROM Drugs d WHERE d.company = :company"),
    @NamedQuery(name = "Drugs.findByOrigin", query = "SELECT d FROM Drugs d WHERE d.origin = :origin"),
    @NamedQuery(name = "Drugs.findByPrice", query = "SELECT d FROM Drugs d WHERE d.price = :price")})
public class Drugs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "drug_id")
    private Integer drugId;
    @Size(max = 255)
    @Column(name = "registed_code")
    private String registedCode;
    @Size(max = 255)
    @Column(name = "drug_name")
    private String drugName;
    @Size(max = 300)
    @Column(name = "company")
    private String company;
    @Size(max = 255)
    @Column(name = "origin")
    private String origin;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @OneToMany(mappedBy = "drugId")
    private List<DetailDrugsPrescriptions> detailDrugsPrescriptionsList;
    @JoinColumn(name = "unit", referencedColumnName = "unit_id")
    @ManyToOne
    private Units unit;

    public Drugs() {
    }

    public Drugs(Integer drugId) {
        this.drugId = drugId;
    }

    public Integer getDrugId() {
        return drugId;
    }

    public void setDrugId(Integer drugId) {
        this.drugId = drugId;
    }

    public String getRegistedCode() {
        return registedCode;
    }

    public void setRegistedCode(String registedCode) {
        this.registedCode = registedCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @XmlTransient
    public List<DetailDrugsPrescriptions> getDetailDrugsPrescriptionsList() {
        return detailDrugsPrescriptionsList;
    }

    public void setDetailDrugsPrescriptionsList(List<DetailDrugsPrescriptions> detailDrugsPrescriptionsList) {
        this.detailDrugsPrescriptionsList = detailDrugsPrescriptionsList;
    }

    public Units getUnit() {
        return unit;
    }

    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drugId != null ? drugId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Drugs)) {
            return false;
        }
        Drugs other = (Drugs) object;
        if ((this.drugId == null && other.drugId != null) || (this.drugId != null && !this.drugId.equals(other.drugId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Drugs{" + "drugId=" + drugId + ", registedCode=" + registedCode + ", drugName=" + drugName + ", company=" + company + ", origin=" + origin + ", price=" + price + ", detailDrugsPrescriptionsList=" + detailDrugsPrescriptionsList + ", unit=" + unit + '}';
    }   
}
