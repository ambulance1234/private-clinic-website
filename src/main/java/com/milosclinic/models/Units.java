/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "units")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Units.findAll", query = "SELECT u FROM Units u"),
    @NamedQuery(name = "Units.findByUnitId", query = "SELECT u FROM Units u WHERE u.unitId = :unitId"),
    @NamedQuery(name = "Units.findByUnitName", query = "SELECT u FROM Units u WHERE u.unitName = :unitName")})
public class Units implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "unit_id")
    private Integer unitId;
    @Size(max = 255)
    @Column(name = "unit_name")
    private String unitName;
    @OneToMany(mappedBy = "unit")
    private List<DetailDrugsPrescriptions> detailDrugsPrescriptionsList;
    @OneToMany(mappedBy = "unit")
    private List<Drugs> drugsList;

    public Units() {
    }

    public Units(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @XmlTransient
    public List<DetailDrugsPrescriptions> getDetailDrugsPrescriptionsList() {
        return detailDrugsPrescriptionsList;
    }

    public void setDetailDrugsPrescriptionsList(List<DetailDrugsPrescriptions> detailDrugsPrescriptionsList) {
        this.detailDrugsPrescriptionsList = detailDrugsPrescriptionsList;
    }

    @XmlTransient
    public List<Drugs> getDrugsList() {
        return drugsList;
    }

    public void setDrugsList(List<Drugs> drugsList) {
        this.drugsList = drugsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (unitId != null ? unitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Units)) {
            return false;
        }
        Units other = (Units) object;
        if ((this.unitId == null && other.unitId != null) || (this.unitId != null && !this.unitId.equals(other.unitId))) {
            return false;
        }
        return true;
    }   

    @Override
    public String toString() {
        return "Units{" + "unitId=" + unitId + ", unitName=" + unitName + ", detailDrugsPrescriptionsList=" + detailDrugsPrescriptionsList + ", drugsList=" + drugsList + '}';
    }    
}
