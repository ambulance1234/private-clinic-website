/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "prescriptions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prescriptions.findAll", query = "SELECT p FROM Prescriptions p"),
    @NamedQuery(name = "Prescriptions.findByPrescriptId", query = "SELECT p FROM Prescriptions p WHERE p.prescriptionsPK.prescriptId = :prescriptId"),
    @NamedQuery(name = "Prescriptions.findByDoctorId", query = "SELECT p FROM Prescriptions p WHERE p.prescriptionsPK.doctorId = :doctorId"),
    @NamedQuery(name = "Prescriptions.findByPatientId", query = "SELECT p FROM Prescriptions p WHERE p.prescriptionsPK.patientId = :patientId"),
    @NamedQuery(name = "Prescriptions.findByDiagnose", query = "SELECT p FROM Prescriptions p WHERE p.diagnose = :diagnose"),
    @NamedQuery(name = "Prescriptions.findByManifestation", query = "SELECT p FROM Prescriptions p WHERE p.manifestation = :manifestation"),
    @NamedQuery(name = "Prescriptions.findByNote", query = "SELECT p FROM Prescriptions p WHERE p.note = :note"),
    @NamedQuery(name = "Prescriptions.findByPrescriptDate", query = "SELECT p FROM Prescriptions p WHERE p.prescriptDate = :prescriptDate"),
    @NamedQuery(name = "Prescriptions.findByCreatedAt", query = "SELECT p FROM Prescriptions p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "Prescriptions.findByUpdatedAt", query = "SELECT p FROM Prescriptions p WHERE p.updatedAt = :updatedAt")})
public class Prescriptions implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PrescriptionsPK prescriptionsPK;
    @Size(max = 500)
    @Column(name = "diagnose")
    private String diagnose;
    @Size(max = 500)
    @Column(name = "manifestation")
    private String manifestation;
    @Size(max = 255)
    @Column(name = "note")
    private String note;
    @Column(name = "prescript_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date prescriptDate;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "prescriptions")
    private DetailDrugsPrescriptions detailDrugsPrescriptions;
    @JoinColumn(name = "doctor_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Accounts accounts;
    @JoinColumn(name = "patient_id", referencedColumnName = "patient_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Patients patients;

    public Prescriptions() {
    }

    public Prescriptions(PrescriptionsPK prescriptionsPK) {
        this.prescriptionsPK = prescriptionsPK;
    }

    public Prescriptions(int prescriptId, int doctorId, int patientId) {
        this.prescriptionsPK = new PrescriptionsPK(prescriptId, doctorId, patientId);
    }

    public PrescriptionsPK getPrescriptionsPK() {
        return prescriptionsPK;
    }

    public void setPrescriptionsPK(PrescriptionsPK prescriptionsPK) {
        this.prescriptionsPK = prescriptionsPK;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getManifestation() {
        return manifestation;
    }

    public void setManifestation(String manifestation) {
        this.manifestation = manifestation;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getPrescriptDate() {
        return prescriptDate;
    }

    public void setPrescriptDate(Date prescriptDate) {
        this.prescriptDate = prescriptDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public DetailDrugsPrescriptions getDetailDrugsPrescriptions() {
        return detailDrugsPrescriptions;
    }

    public void setDetailDrugsPrescriptions(DetailDrugsPrescriptions detailDrugsPrescriptions) {
        this.detailDrugsPrescriptions = detailDrugsPrescriptions;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prescriptionsPK != null ? prescriptionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prescriptions)) {
            return false;
        }
        Prescriptions other = (Prescriptions) object;
        if ((this.prescriptionsPK == null && other.prescriptionsPK != null) || (this.prescriptionsPK != null && !this.prescriptionsPK.equals(other.prescriptionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.phunghung.pojo.Prescriptions[ prescriptionsPK=" + prescriptionsPK + " ]";
    }
    
}
