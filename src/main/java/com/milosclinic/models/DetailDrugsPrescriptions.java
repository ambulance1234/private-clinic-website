/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "detail_drugs_prescriptions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailDrugsPrescriptions.findAll", query = "SELECT d FROM DetailDrugsPrescriptions d"),
    @NamedQuery(name = "DetailDrugsPrescriptions.findByPrescriptId", query = "SELECT d FROM DetailDrugsPrescriptions d WHERE d.detailDrugsPrescriptionsPK.prescriptId = :prescriptId"),
    @NamedQuery(name = "DetailDrugsPrescriptions.findByDoctorId", query = "SELECT d FROM DetailDrugsPrescriptions d WHERE d.detailDrugsPrescriptionsPK.doctorId = :doctorId"),
    @NamedQuery(name = "DetailDrugsPrescriptions.findByPatientId", query = "SELECT d FROM DetailDrugsPrescriptions d WHERE d.detailDrugsPrescriptionsPK.patientId = :patientId"),
    @NamedQuery(name = "DetailDrugsPrescriptions.findByPrice", query = "SELECT d FROM DetailDrugsPrescriptions d WHERE d.price = :price"),
    @NamedQuery(name = "DetailDrugsPrescriptions.findByQuantity", query = "SELECT d FROM DetailDrugsPrescriptions d WHERE d.quantity = :quantity"),
    @NamedQuery(name = "DetailDrugsPrescriptions.findByUsePerDay", query = "SELECT d FROM DetailDrugsPrescriptions d WHERE d.usePerDay = :usePerDay"),
    @NamedQuery(name = "DetailDrugsPrescriptions.findByQuantityPerUse", query = "SELECT d FROM DetailDrugsPrescriptions d WHERE d.quantityPerUse = :quantityPerUse")})
public class DetailDrugsPrescriptions implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetailDrugsPrescriptionsPK detailDrugsPrescriptionsPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Size(max = 255)
    @Column(name = "quantity")
    private String quantity;
    @Column(name = "use_per_day")
    private Integer usePerDay;
    @Column(name = "quantity_per_use")
    private Integer quantityPerUse;
    @JoinColumn(name = "drug_id", referencedColumnName = "drug_id")
    @ManyToOne
    private Drugs drugId;
    @JoinColumn(name = "use_in_part", referencedColumnName = "part_id")
    @ManyToOne
    private PartsOfDay useInPart;
    @JoinColumns({
        @JoinColumn(name = "prescript_id", referencedColumnName = "prescript_id", insertable = false, updatable = false),
        @JoinColumn(name = "doctor_id", referencedColumnName = "doctor_id", insertable = false, updatable = false),
        @JoinColumn(name = "patient_id", referencedColumnName = "patient_id", insertable = false, updatable = false)})
    @OneToOne(optional = false)
    private Prescriptions prescriptions;
    @JoinColumn(name = "unit", referencedColumnName = "unit_id")
    @ManyToOne
    private Units unit;

    public DetailDrugsPrescriptions() {
    }

    public DetailDrugsPrescriptions(DetailDrugsPrescriptionsPK detailDrugsPrescriptionsPK) {
        this.detailDrugsPrescriptionsPK = detailDrugsPrescriptionsPK;
    }

    public DetailDrugsPrescriptions(int prescriptId, int doctorId, int patientId, int detailId) {
        this.detailDrugsPrescriptionsPK = new DetailDrugsPrescriptionsPK(prescriptId, doctorId, patientId, detailId);
    }

    public DetailDrugsPrescriptionsPK getDetailDrugsPrescriptionsPK() {
        return detailDrugsPrescriptionsPK;
    }

    public void setDetailDrugsPrescriptionsPK(DetailDrugsPrescriptionsPK detailDrugsPrescriptionsPK) {
        this.detailDrugsPrescriptionsPK = detailDrugsPrescriptionsPK;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Integer getUsePerDay() {
        return usePerDay;
    }

    public void setUsePerDay(Integer usePerDay) {
        this.usePerDay = usePerDay;
    }

    public Integer getQuantityPerUse() {
        return quantityPerUse;
    }

    public void setQuantityPerUse(Integer quantityPerUse) {
        this.quantityPerUse = quantityPerUse;
    }

    public Drugs getDrugId() {
        return drugId;
    }

    public void setDrugId(Drugs drugId) {
        this.drugId = drugId;
    }

    public PartsOfDay getUseInPart() {
        return useInPart;
    }

    public void setUseInPart(PartsOfDay useInPart) {
        this.useInPart = useInPart;
    }

    public Prescriptions getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Prescriptions prescriptions) {
        this.prescriptions = prescriptions;
    }

    public Units getUnit() {
        return unit;
    }

    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detailDrugsPrescriptionsPK != null ? detailDrugsPrescriptionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailDrugsPrescriptions)) {
            return false;
        }
        DetailDrugsPrescriptions other = (DetailDrugsPrescriptions) object;
        if ((this.detailDrugsPrescriptionsPK == null && other.detailDrugsPrescriptionsPK != null) || (this.detailDrugsPrescriptionsPK != null && !this.detailDrugsPrescriptionsPK.equals(other.detailDrugsPrescriptionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.phunghung.pojo.DetailDrugsPrescriptions[ detailDrugsPrescriptionsPK=" + detailDrugsPrescriptionsPK + " ]";
    }
    
}
