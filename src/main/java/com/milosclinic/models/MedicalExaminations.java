/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "medical_examinations")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedicalExaminations.findAll", query = "SELECT m FROM MedicalExaminations m"),
    @NamedQuery(name = "MedicalExaminations.findByExamId", query = "SELECT m FROM MedicalExaminations m WHERE m.medicalExaminationsPK.examId = :examId"),
    @NamedQuery(name = "MedicalExaminations.findByDoctorId", query = "SELECT m FROM MedicalExaminations m WHERE m.medicalExaminationsPK.doctorId = :doctorId"),
    @NamedQuery(name = "MedicalExaminations.findByPatientId", query = "SELECT m FROM MedicalExaminations m WHERE m.medicalExaminationsPK.patientId = :patientId"),
    @NamedQuery(name = "MedicalExaminations.findByExamDate", query = "SELECT m FROM MedicalExaminations m WHERE m.examDate = :examDate"),
    @NamedQuery(name = "MedicalExaminations.findByReason", query = "SELECT m FROM MedicalExaminations m WHERE m.reason = :reason"),
    @NamedQuery(name = "MedicalExaminations.findByCreatedAt", query = "SELECT m FROM MedicalExaminations m WHERE m.createdAt = :createdAt"),
    @NamedQuery(name = "MedicalExaminations.findByUpdatedAt", query = "SELECT m FROM MedicalExaminations m WHERE m.updatedAt = :updatedAt")})
public class MedicalExaminations implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MedicalExaminationsPK medicalExaminationsPK;
    @Column(name = "exam_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date examDate;
    @Size(max = 255)
    @Column(name = "reason")
    private String reason;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @JoinColumn(name = "doctor_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Accounts accounts;
    @JoinColumn(name = "patient_id", referencedColumnName = "patient_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Patients patients;

    public MedicalExaminations() {
    }

    public MedicalExaminations(MedicalExaminationsPK medicalExaminationsPK) {
        this.medicalExaminationsPK = medicalExaminationsPK;
    }

    public MedicalExaminations(int examId, int doctorId, int patientId) {
        this.medicalExaminationsPK = new MedicalExaminationsPK(examId, doctorId, patientId);
    }

    public MedicalExaminationsPK getMedicalExaminationsPK() {
        return medicalExaminationsPK;
    }

    public void setMedicalExaminationsPK(MedicalExaminationsPK medicalExaminationsPK) {
        this.medicalExaminationsPK = medicalExaminationsPK;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medicalExaminationsPK != null ? medicalExaminationsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicalExaminations)) {
            return false;
        }
        MedicalExaminations other = (MedicalExaminations) object;
        if ((this.medicalExaminationsPK == null && other.medicalExaminationsPK != null) || (this.medicalExaminationsPK != null && !this.medicalExaminationsPK.equals(other.medicalExaminationsPK))) {
            return false;
        }
        return true;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.milosclinic.models.MedicalExaminations[ medicalExaminationsPK=" + medicalExaminationsPK + " ]";
    }

//    @Override
//    public String toString() {
//        return "MedicalExaminations[" + "medicalExaminationsPK=" + medicalExaminationsPK + ", examDate=" + examDate + ", reason=" + reason + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", accounts=" + accounts + ", patients=" + patients + ']';
//    }

}
