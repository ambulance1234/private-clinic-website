/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "parts_of_day")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PartsOfDay.findAll", query = "SELECT p FROM PartsOfDay p"),
    @NamedQuery(name = "PartsOfDay.findByPartId", query = "SELECT p FROM PartsOfDay p WHERE p.partId = :partId"),
    @NamedQuery(name = "PartsOfDay.findByPartName", query = "SELECT p FROM PartsOfDay p WHERE p.partName = :partName")})
public class PartsOfDay implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "part_id")
    private Integer partId;
    @Size(max = 255)
    @Column(name = "part_name")
    private String partName;
    @OneToMany(mappedBy = "useInPart")
    private List<DetailDrugsPrescriptions> detailDrugsPrescriptionsList;

    public PartsOfDay() {
    }

    public PartsOfDay(Integer partId) {
        this.partId = partId;
    }

    public Integer getPartId() {
        return partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    @XmlTransient
    public List<DetailDrugsPrescriptions> getDetailDrugsPrescriptionsList() {
        return detailDrugsPrescriptionsList;
    }

    public void setDetailDrugsPrescriptionsList(List<DetailDrugsPrescriptions> detailDrugsPrescriptionsList) {
        this.detailDrugsPrescriptionsList = detailDrugsPrescriptionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (partId != null ? partId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PartsOfDay)) {
            return false;
        }
        PartsOfDay other = (PartsOfDay) object;
        if ((this.partId == null && other.partId != null) || (this.partId != null && !this.partId.equals(other.partId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.phunghung.pojo.PartsOfDay[ partId=" + partId + " ]";
    }
    
}
