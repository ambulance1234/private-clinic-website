/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "genders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Genders.findAll", query = "SELECT g FROM Genders g"),
    @NamedQuery(name = "Genders.findByGenderId", query = "SELECT g FROM Genders g WHERE g.genderId = :genderId"),
    @NamedQuery(name = "Genders.findByGenderName", query = "SELECT g FROM Genders g WHERE g.genderName = :genderName")})
public class Genders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gender_id")
    private Integer genderId;
    @Size(max = 255)
    @Column(name = "gender_name")
    private String genderName;
    @OneToMany(mappedBy = "genderId")
    private List<Patients> patientsList;
    @OneToMany(mappedBy = "genderId")
    private List<Accounts> accountsList;

    public Genders() {
    }

    public Genders(Integer genderId) {
        this.genderId = genderId;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    @XmlTransient
    public List<Patients> getPatientsList() {
        return patientsList;
    }

    public void setPatientsList(List<Patients> patientsList) {
        this.patientsList = patientsList;
    }

    @XmlTransient
    public List<Accounts> getAccountsList() {
        return accountsList;
    }

    public void setAccountsList(List<Accounts> accountsList) {
        this.accountsList = accountsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genderId != null ? genderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Genders)) {
            return false;
        }
        Genders other = (Genders) object;
        if ((this.genderId == null && other.genderId != null) || (this.genderId != null && !this.genderId.equals(other.genderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Genders{" + "genderId=" + genderId + ", genderName=" + genderName + ", patientsList=" + patientsList + ", accountsList=" + accountsList + '}';
    }

    
    
}
