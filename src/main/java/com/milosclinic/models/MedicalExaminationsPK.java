/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ACER
 */
@Embeddable
public class MedicalExaminationsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "exam_id")
    private int examId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "doctor_id")
    private int doctorId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "patient_id")
    private int patientId;

    public MedicalExaminationsPK() {
    }

    public MedicalExaminationsPK(int examId, int doctorId, int patientId) {
        this.examId = examId;
        this.doctorId = doctorId;
        this.patientId = patientId;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) examId;
        hash += (int) doctorId;
        hash += (int) patientId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicalExaminationsPK)) {
            return false;
        }
        MedicalExaminationsPK other = (MedicalExaminationsPK) object;
        if (this.examId != other.examId) {
            return false;
        }
        if (this.doctorId != other.doctorId) {
            return false;
        }
        if (this.patientId != other.patientId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.phunghung.pojo.MedicalExaminationsPK[ examId=" + examId + ", doctorId=" + doctorId + ", patientId=" + patientId + " ]";
    }
    
}
