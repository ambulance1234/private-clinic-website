/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ACER
 */
public class RequestParser {

    /**
     *
     * @param <T>
     * @param request
     * @param finalObj
     * @return
     */
    public static <T> T requestBodyHandler(HttpServletRequest request, Class<T> finalObj) {
        try {
            String jsonString = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            return mapJsonToObject(jsonString, finalObj);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * 
     * @param <T>
     * @param <V>
     * @param request
     * @param dtoClass
     * @param modelClass
     * @param modelInstance
     * @return
     */
    public static <T, V> V getBodyContentAsModelClass(HttpServletRequest request, Class<T> dtoClass, Class<V> modelClass, Supplier<V> modelInstance) {
        T dto = requestBodyHandler(request, dtoClass);
        V model = AutoMapping.mapTo(dtoClass, dto, modelClass, modelInstance );
        return model;
    }

    /**
     *
     * @param <T>
     * @param json
     * @param instance
     * @return
     */
    public static <T> T mapJsonToObject(String json, Class<T> instance) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            T result = mapper.readValue(json, instance);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
