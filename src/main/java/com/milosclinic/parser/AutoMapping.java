/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.parser;

/**
 *
 * @author ACER
 */
import java.lang.reflect.Field;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AutoMapping {

    public static <F, T> T mapTo(Class<F> modelClass, F origin, Class<T> destinyClass, Supplier<T> destinyNewInstance) {

        try {
            
            T destiny = destinyNewInstance.get();
            
            Field[] fieldsOfOrigin = modelClass.getDeclaredFields();
            Field[] fieldsOfDestiny = destinyClass.getDeclaredFields();
            for (Field fieldOrigin : fieldsOfOrigin) {
                fieldOrigin.setAccessible(true);
                for (Field fieldDestiny : fieldsOfDestiny) {
                    fieldDestiny.setAccessible(true);
                    if (fieldOrigin.getName().equals(fieldDestiny.getName())) {
                        fieldDestiny.set(destiny, fieldOrigin.get(origin));
                    }
                }
            }
            return destiny;
        } catch (IllegalArgumentException | IllegalAccessException  e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    public static <F, T> T mapToUsingJson(Class<F> modelClass, F model, Class<T> destiny) {
        try {

            JSONObject jsonObject = new JSONObject();
//			System.out.println("MapTo function");
            Field[] fields = modelClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
//				System.out.println("Field: " + field.getName() + " & Value: " + field.get(model));
                jsonObject.put(field.getName(), field.get(model));
            }
            ObjectMapper mapper = new ObjectMapper();
            T destiny_ = mapper.readValue(jsonObject.toString(), destiny);

            return destiny_;

        } catch (IllegalArgumentException | IllegalAccessException | JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

    }

}
