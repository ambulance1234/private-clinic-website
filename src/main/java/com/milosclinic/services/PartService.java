/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IPartRepo;
import com.milosclinic.interfaces.services.IPartService;
import com.milosclinic.models.PartsOfDay;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ACER
 */
@Service
public class PartService implements IPartService{
    
    @Autowired
    private IPartRepo repo;

    @Override
    public List<PartsOfDay> findAll() {
        return repo.findAll();
    }

    @Override
    public PartsOfDay findOneById(int id) {
        return repo.findOneById(id);
    }
    
}
