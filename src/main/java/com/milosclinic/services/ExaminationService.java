/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IExaminationRepo;
import com.milosclinic.interfaces.services.IExaminationService;
import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.MedicalExaminationsPK;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class ExaminationService implements IExaminationService {

    @Autowired
    private IExaminationRepo repo;

    @Override
    public List<MedicalExaminations> findAll() {
        return null;
    }

    @Override
    public List<MedicalExaminations> findOneById(MedicalExaminationsPK target) {
        return repo.findOneById(target);
    }

    @Override
    public long create(MedicalExaminations newOne) {
        var list = repo.findAll();

        list = list.stream().filter(item
                -> item.getMedicalExaminationsPK().getDoctorId() == newOne.getMedicalExaminationsPK().getDoctorId()
        ).collect(Collectors.toList());

        if (list == null || list.isEmpty()) {
            newOne.getMedicalExaminationsPK().setExamId(1);
            return repo.create(newOne);
        }

        Calendar calendar = Calendar.getInstance();
        Calendar xCalendar = Calendar.getInstance();
        xCalendar.setTime(newOne.getExamDate());
        list = list.stream().filter((MedicalExaminations item) -> {
            calendar.setTime(item.getExamDate());
            return calendar.get(Calendar.YEAR) == xCalendar.get(Calendar.YEAR)
                    && calendar.get(Calendar.MONTH) == xCalendar.get(Calendar.MONTH)
                    && calendar.get(Calendar.DAY_OF_MONTH) == xCalendar.get(Calendar.DAY_OF_MONTH);
        }).collect(Collectors.toList());
        long existed = list.stream().filter(item -> {
            calendar.setTime(item.getExamDate());
            return (calendar.get(Calendar.HOUR_OF_DAY) == xCalendar.get(Calendar.HOUR_OF_DAY)
                    && calendar.get(Calendar.HOUR_OF_DAY) <= (xCalendar.get(Calendar.HOUR_OF_DAY) + 1))
                    && (calendar.get(Calendar.MINUTE) == xCalendar.get(Calendar.MINUTE));
        }).count();
        if (existed == 0) {
            list = repo.findOneById(newOne.getMedicalExaminationsPK());
            int lastExamId = list.get(list.size() - 1).getMedicalExaminationsPK().getExamId();
            lastExamId += 1;
            newOne.getMedicalExaminationsPK().setExamId(lastExamId);
            return repo.create(newOne);
        }
        return 0;
    }

    @Override
    public int update(MedicalExaminations existed) {
        return 0;
    }

    @Override
    public List<MedicalExaminations> findByPatientId(int id) {
        var list = repo.findByPatientId(id);
//        list.forEach(item -> {
//            System.out.println(item);
//            System.out.println("-----item: " + item.toString());
//        });
        return list;
    }

    @Override
    public int delete(MedicalExaminations exam) {
        return repo.delete(exam);
    }

}
