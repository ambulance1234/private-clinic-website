/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.milosclinic.interfaces.repositories.IAccountRepo;
import com.milosclinic.interfaces.repositories.IGenderRepo;
import com.milosclinic.interfaces.repositories.IRoleRepo;
import com.milosclinic.interfaces.repositories.ITagRepo;
import com.milosclinic.interfaces.services.IAccountService;
import com.milosclinic.models.Accounts;
import com.milosclinic.repositories.AccountRepo;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author ACER
 */
@Service("userDetailsService")
public class AccountService implements IAccountService {

    @Autowired
    private IGenderRepo genderRepo;
    
    @Autowired
    private ITagRepo tagRepo;
    
    @Autowired
    private IRoleRepo roleRepo;

    @Autowired
    private IAccountRepo repository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private Cloudinary cloudinary;

//    public AccountService(AccountRepo repo){
//        this.repository = repo;
//    }
    @Override
    public List<Accounts> findAll() {
        return repository.findAll();
    }

    @Override
    public Accounts findOneByID(int id) {
        return repository.findOneByID(id);
    }
    
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Accounts accounts = this.repository.findOneByEmail(email);
        if (accounts == null) {
            throw new UsernameNotFoundException("Invalid account");
        }
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(accounts.getRoles().getRoleName()));

        return new org.springframework.security.core.userdetails.User(accounts.getEmail(), accounts.getPassword(), authorities);
    }

    @Override
    public Accounts findOneByEmail(String email) {
        return this.repository.findOneByEmail(email);
    }
    

    @Override
    public List<Accounts> findAllNurses() {
        return repository.findAllNurses();
    }

    @Override
    public List<Accounts> findAllDoctors() {
        return repository.findAllDoctors();
    }

    @Override
    public List<Accounts> findAllAdmins() {
        return repository.findAllAdmins();
    }

    @Override
    public List<Accounts> filter(String keyword) {
        return repository.filter(keyword);
    }
    
    @Override
    public boolean createAccount(Accounts accounts) {
        Map res = null;
        if (accounts.getAvatar() != null) {
            try {
                res = cloudinary.uploader().upload(accounts.getAvatar().getBytes(),
                        ObjectUtils.asMap("resource_type", "image"));
                if (res == null) {
                    return false;
                }
                accounts.setImageSource((String) res.get("secure_url"));
                System.out.println(res);

                accounts.setPassword(this.passwordEncoder.encode(accounts.getPassword()));

                var gender = genderRepo.findOneByName(accounts.getGenderId().getGenderName());
                accounts.getGenderId().setGenderId(gender.getGenderId());
                
//                var tag = tagRepo.findOneById(accounts.getTagId().getTagId());                
//                accounts.getTagId().setName(tag.getName());
//                
//                var role = roleRepo.findOneById(accounts.getRoles().getRoleId());
//                accounts.getRoles().setRoleName(role.getRoleName());
                
                accounts.setCreatedAt(new Date());
                accounts.setUpdatedAt(new Date());

                return this.repository.createAccount(accounts);
            } catch (IOException ex) {
//                 Logger.getLogger()
//                System.out.println(ex.getMessage());
                ex.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
    
    

}
