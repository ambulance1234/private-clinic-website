/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IDetailPrescriptRepo;
import com.milosclinic.interfaces.services.IDetailService;
import com.milosclinic.models.DetailDrugsPrescriptions;
import com.milosclinic.models.PrescriptionsPK;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ACER
 */
@Service
public class DetailService implements IDetailService{
    
    @Autowired
    private IDetailPrescriptRepo detailRepo;

    @Override
    public List<DetailDrugsPrescriptions> findByPrescript(PrescriptionsPK prescriptionsPK) {
        return detailRepo.findByPrescript(prescriptionsPK);
    }
    
    @Override
    public int create(DetailDrugsPrescriptions detail) {
        var oldList = detailRepo.findByPrescript(
                new PrescriptionsPK(
                        detail.getDetailDrugsPrescriptionsPK().getPrescriptId(), 
                        detail.getDetailDrugsPrescriptionsPK().getDoctorId(), 
                        detail.getDetailDrugsPrescriptionsPK().getPatientId()
                )
        );
        
        if(oldList == null) {
            detail.getDetailDrugsPrescriptionsPK().setDetailId(1);
        }
        else {
            int lastDetailId = oldList.get(0).getDetailDrugsPrescriptionsPK().getDetailId();
            detail.getDetailDrugsPrescriptionsPK().setDetailId(lastDetailId + 1);
        }
        
        return detailRepo.create(detail);
    }

    @Override
    public int create(List<DetailDrugsPrescriptions> detail) {        
        return 0;
    }

    @Override
    public int update(DetailDrugsPrescriptions detail) {
        return detailRepo.update(detail);
    }

    @Override
    public int delete(DetailDrugsPrescriptions detail) {
        return detailRepo.delete(detail);
    }    
}
