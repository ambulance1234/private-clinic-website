/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IRoleRepo;
import com.milosclinic.interfaces.services.IRoleService;
import com.milosclinic.models.Roles;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class RoleService implements IRoleService{
    
    @Autowired
    private IRoleRepo roleRepo;

    @Override
    public List<Roles> findAll() {
        return roleRepo.findAll();
    }

    @Override
    public Roles findOneById(int id) {
        return roleRepo.findOneById(id);
    }
    
}
