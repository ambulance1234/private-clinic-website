/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IGenderRepo;
import com.milosclinic.interfaces.repositories.IPatientRepo;
import com.milosclinic.interfaces.services.IPatientService;
import com.milosclinic.models.Genders;
import com.milosclinic.models.Patients;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ACER
 */
@Service
public class PatientService implements IPatientService {

    @Autowired
    private IPatientRepo repo;

    @Autowired
    private IGenderRepo genderRepo;

    @Override
    public List<Patients> findAll() {
        var list = repo.findAll();
        list.forEach(item -> {
            var formatGender = new Genders();
            int genderId = item.getGenderId().getGenderId();
            String genderName = item.getGenderId().getGenderName().equals("Male") ? "M" : "F";
            formatGender.setGenderId(genderId);
            formatGender.setGenderName(genderName);
            item.setGenderId(formatGender);
        });
        return list;
    }

    @Override
    public Patients findOneByID(int id) {
        var patient = repo.findOneByID(id);
        var formatGender = new Genders();
        int genderId = patient.getGenderId().getGenderId();
        String genderName = patient.getGenderId().getGenderName().equals("Male") ? "M" : "F";
        formatGender.setGenderId(genderId);
        formatGender.setGenderName(genderName);
        patient.setGenderId(formatGender);
        return patient;
    }

    @Override
    public List<Patients> filter(String keyword) {
        return repo.filter(keyword);
    }

    @Override
    public long create(Patients patient) {
        //check existed patient
        if (patient.getNationalId() == null) {
            return 0;
        }
        var existed = repo.findOneByNationalID(patient.getNationalId());
        if (existed != null) {
            return -1;
        }

        patient = translateGender(patient);

        if (patient == null) {
            return -2;
        }

        patient.setCreatedAt(new Date());
        patient.setUpdatedAt(new Date());

        return repo.create(patient);
    }

    @Override
    public int update(Patients patient) {

        patient = translateGender(patient);

        if (patient == null) {
            return -1;
        }

        return repo.update(patient);
    }

    @Override
    public int delete(Patients patient) {
        return repo.delete(patient);
    }

    private Patients translateGender(Patients patient) {
        String genderOfPatient = patient.getGenderId().getGenderName().toLowerCase();

        if (genderOfPatient.compareTo("m") == 0 || genderOfPatient.compareTo("male") == 0 || genderOfPatient.compareTo("nam") == 0) {
            patient.getGenderId().setGenderId(
                    genderRepo.findOneByName("Male").getGenderId()
            );
        } else if (genderOfPatient.compareTo("f") == 0 || genderOfPatient.compareTo("female") == 0 || genderOfPatient.compareTo("nu") == 0) {
            patient.getGenderId().setGenderId(
                    genderRepo.findOneByName("Female").getGenderId()
            );
        } else {
            return null;
        }
        return patient;

    }

}
