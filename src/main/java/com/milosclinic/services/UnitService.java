/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IUnitRepo;
import com.milosclinic.interfaces.services.IUnitService;
import com.milosclinic.models.Units;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ACER
 */
@Service
public class UnitService implements IUnitService{

    @Autowired
    private IUnitRepo unitRepo;
    
    @Override
    public List<Units> findAll() {
        return unitRepo.findAll();
    }

    @Override
    public Units findOneById(int id) {
        return unitRepo.findOneById(id);
    }
    
}
