/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IPrescriptRepo;
import com.milosclinic.interfaces.services.IPrescriptionService;
import com.milosclinic.models.Prescriptions;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class PrescriptionService implements IPrescriptionService {

    @Autowired
    private IPrescriptRepo repo;

    @Override
    public List<Prescriptions> findAllByPatient(int patientId) {
        return repo.findAllByPatient(patientId);
    }

    @Override
    public Prescriptions findOneById(int patientId, int doctorId, int prescriptId) {
        return repo.findOneById(patientId, doctorId, prescriptId);
    }

    @Override
    public long create(Prescriptions newOne) {
        var theLast = repo.findTheLastOfPatientWithDoctor(
                newOne.getPrescriptionsPK().getPatientId(), 
                newOne.getPrescriptionsPK().getDoctorId()
        );
        
        if(theLast == null) {
            newOne.getPrescriptionsPK().setPrescriptId(1);
        }
        else {
            newOne.getPrescriptionsPK().setPrescriptId(theLast.getPrescriptionsPK().getPrescriptId() + 1);
        }
        
        newOne.setPrescriptDate(new Date());
        newOne.setCreatedAt(new Date());
        newOne.setUpdatedAt(new Date());
        
        return repo.create(newOne);
    }

    @Override
    public int update(Prescriptions existed) {
        existed.setUpdatedAt(new Date());
        return repo.update(existed);
    }

    @Override
    public int delete(Prescriptions existed) {
        return repo.delete(existed);
    }
    
    
}
