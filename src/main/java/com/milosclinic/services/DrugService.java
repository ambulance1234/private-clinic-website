/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.IDrugRepo;
import com.milosclinic.interfaces.services.IDrugService;
import com.milosclinic.models.Drugs;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ACER
 */
@Service
public class DrugService implements IDrugService{
    
    @Autowired
    private IDrugRepo repo;

    @Override
    public List<Drugs> findAll() {
        return repo.findAll();
    }

    @Override
    public Drugs findOneByID(int id) {
        return repo.findOneByID(id);
    }

    @Override
    public Drugs findOneByRegistedCode(String code) {
        return repo.findOneByRegistedCode(code);
    }

    @Override
    public int create(Drugs drug) {
        Drugs existed = repo.findOneByRegistedCode(drug.getRegistedCode());
        if(existed != null) {
            return 0;
        }
        
        return repo.create(drug);
    }

    @Override
    public int update(Drugs drug) {
        return repo.update(drug);
    }

    @Override
    public int delete(Drugs drug) {
        return repo.delete(drug);
    }
    
}
