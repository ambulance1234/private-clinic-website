/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.services;

import com.milosclinic.interfaces.repositories.ITagRepo;
import com.milosclinic.interfaces.services.ITagService;
import com.milosclinic.models.Tags;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ACER
 */
@Service
public class TagService implements ITagService{

    @Autowired
    private ITagRepo tagRepo;
    
    @Override
    public List<Tags> findAll() {
        return tagRepo.findAll();
    }

    @Override
    public Tags findOneById(int id) {
        return tagRepo.findOneById(id);
    }
    
}
