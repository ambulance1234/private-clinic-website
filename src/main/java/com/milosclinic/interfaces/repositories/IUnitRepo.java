/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.Units;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface IUnitRepo {
    List<Units> findAll();
    Units findOneById(int id);   
}
