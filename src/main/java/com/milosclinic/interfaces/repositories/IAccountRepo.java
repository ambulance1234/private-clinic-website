/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.Accounts;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface IAccountRepo {
    List<Accounts> findAll();
    Accounts findOneByID(int id);
    Accounts findOneByEmail(String email);
    List<Accounts> findAllNurses();
    List<Accounts> findAllDoctors();
    List<Accounts> findAllAdmins();
    List<Accounts> filter(String keyword);
    boolean createAccount(Accounts accounts);
//    Accounts getUserByUsername(String accounts);
    
}
