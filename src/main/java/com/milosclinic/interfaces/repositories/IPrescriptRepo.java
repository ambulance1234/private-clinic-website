/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.Prescriptions;
import com.milosclinic.models.PrescriptionsPK;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface IPrescriptRepo {
    List<Prescriptions> findAllByPatient(int patientId);
    Prescriptions findTheLastOfPatientWithDoctor(int patientId, int doctorId);
    Prescriptions findOneById(int patientId, int doctorId, int prescriptId);
    long create(Prescriptions newOne);
    int update(Prescriptions existed);
    int delete(Prescriptions existed);
}
