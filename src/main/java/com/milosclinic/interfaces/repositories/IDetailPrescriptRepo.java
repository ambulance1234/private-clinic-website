/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.DetailDrugsPrescriptions;
import com.milosclinic.models.PrescriptionsPK;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface IDetailPrescriptRepo {
    List<DetailDrugsPrescriptions> findByPrescript(PrescriptionsPK prescriptionsPK);
    int create(DetailDrugsPrescriptions detail);
    int create(List<DetailDrugsPrescriptions> details);
    int update(DetailDrugsPrescriptions detail);
    int delete(DetailDrugsPrescriptions detail);
}
