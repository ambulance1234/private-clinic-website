/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.PartsOfDay;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface IPartRepo {
    List<PartsOfDay> findAll();
    PartsOfDay findOneById(int id);
}
