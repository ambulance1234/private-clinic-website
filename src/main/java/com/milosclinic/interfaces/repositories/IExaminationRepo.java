/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.MedicalExaminationsPK;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface IExaminationRepo {
    List<MedicalExaminations> findAll();
    List<MedicalExaminations> findOneById(MedicalExaminationsPK target);
    List<MedicalExaminations> findByPatientId(int patientId);
    long create(MedicalExaminations newOne);
    int update(MedicalExaminations existed);    
    int delete(MedicalExaminations existed);
}
