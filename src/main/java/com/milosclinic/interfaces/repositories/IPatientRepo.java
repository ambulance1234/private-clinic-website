/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.Patients;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface IPatientRepo {
    List<Patients> findAll();
    Patients findOneByID(int id);
    Patients findOneByNationalID(String nid);
    List<Patients> filter(String keyword);
    long create(Patients patient);
    int update(Patients patient);
    int delete(Patients patient);
}
