/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.interfaces.repositories;

import com.milosclinic.models.Tags;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface ITagRepo {
    List<Tags> findAll();
    Tags findOneById(int id);    
}
