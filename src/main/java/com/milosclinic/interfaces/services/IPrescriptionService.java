/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.interfaces.services;

import com.milosclinic.models.Prescriptions;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface IPrescriptionService {
    
    List<Prescriptions> findAllByPatient(int patientId);
    Prescriptions findOneById(int patientId, int doctorId, int prescriptId);
    long create(Prescriptions newOne);
    int update(Prescriptions existed);
    int delete(Prescriptions existed);
    
}
