/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.interfaces.services;

import com.milosclinic.models.Drugs;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface IDrugService {
    List<Drugs> findAll();
    Drugs findOneByID(int id);
    Drugs findOneByRegistedCode(String code);
    int create(Drugs drug);
    int update(Drugs drug);
    int delete(Drugs drug);
}
