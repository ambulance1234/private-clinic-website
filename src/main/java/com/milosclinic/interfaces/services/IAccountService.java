/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.interfaces.services;

import com.milosclinic.models.Accounts;
import java.util.List;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author ACER
 */
public interface IAccountService extends UserDetailsService{
    List<Accounts> findAll();
    Accounts findOneByID(int id);
    List<Accounts> findAllNurses();
    List<Accounts> findAllDoctors();
    List<Accounts> findAllAdmins();
    List<Accounts> filter(String keyword);
    Accounts findOneByEmail(String email);
    boolean createAccount(Accounts accounts);
}
