/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.interfaces.services;
import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.MedicalExaminationsPK;

import java.util.List;

/**
 *
 * @author Admin
 */
public interface IExaminationService {
    List<MedicalExaminations> findAll();
    List<MedicalExaminations> findOneById(MedicalExaminationsPK target);
    long create(MedicalExaminations newOne);
    int update(MedicalExaminations existed);
    List<MedicalExaminations> findByPatientId(int id);
    int delete(MedicalExaminations exam);
}
