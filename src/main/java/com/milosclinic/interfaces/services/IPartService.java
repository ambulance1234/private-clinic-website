/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.milosclinic.interfaces.services;

import com.milosclinic.models.PartsOfDay;
import java.util.List;

/**
 *
 * @author ACER
 */
public interface IPartService {
    List<PartsOfDay> findAll();
    PartsOfDay findOneById(int id);
}
