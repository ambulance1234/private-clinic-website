/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.interfaces.services;

import com.milosclinic.models.Roles;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface IRoleService {
    List<Roles> findAll();
    Roles findOneById(int id);
}
