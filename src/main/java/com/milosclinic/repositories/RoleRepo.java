/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IRoleRepo;
import com.milosclinic.models.Roles;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Admin
 */
@Repository
@Transactional
public class RoleRepo implements IRoleRepo{
    
    @Autowired
    private LocalSessionFactoryBean sessionFactory;

    @Override
    public List<Roles> findAll() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Roles.findAll", Roles.class);
            
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Roles findOneById(int id) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Roles.findByRoleId", Roles.class);
            
            return (Roles) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
