/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IDrugRepo;
import com.milosclinic.models.Drugs;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ACER
 */
@Repository
@Transactional
public class DrugRepo implements IDrugRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;

    @Override
    public List<Drugs> findAll() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Drugs.findAll", Drugs.class);
            List<Drugs> result = query.getResultList();
            return result;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Drugs findOneByID(int id) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Drugs.findByDrugId", Drugs.class);
            query.setParameter("drugId", id);
            return (Drugs) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Drugs findOneByRegistedCode(String code) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Drugs.findByRegistedCode", Drugs.class);
            query.setParameter("registedCode", code);
            return (Drugs) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int create(Drugs drug) {
        try {
            sessionFactory.getObject().getCurrentSession().save(drug);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int update(Drugs drug) {
        try {
            sessionFactory.getObject().getCurrentSession().update(drug);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int delete(Drugs drug) {
        try {
            sessionFactory.getObject().getCurrentSession().delete(drug);
            return 1;
        } catch (HibernateException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
