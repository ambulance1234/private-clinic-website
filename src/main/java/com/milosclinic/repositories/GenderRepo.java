/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IGenderRepo;
import com.milosclinic.models.Genders;
import java.util.List;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ACER
 */
@Repository
@Transactional
public class GenderRepo implements IGenderRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;
    
    /**
     *
     * @return List of Genders
     */
    @Override
    public List<Genders> findAll() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Genders.findAll", Genders.class);
            var genders = query.getResultList();
            return genders;
        } catch (HibernateException e) {
            return null;
        }
        
    }

    /**
     *
     * @param name
     * @return Genders
     */
    @Override
    public Genders findOneByName(String name) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Genders.findByGenderName", Genders.class);
            query.setParameter("genderName", name);
            var genders = (Genders) query.getSingleResult();
            return genders;
        } catch (HibernateException e) {
            return null;
        }
    }
    
}
