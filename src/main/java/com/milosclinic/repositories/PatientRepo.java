/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IPatientRepo;
import com.milosclinic.models.Patients;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ACER
 */
@Repository
@Transactional
public class PatientRepo implements IPatientRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;

    /**
     *
     * @return
     */
    @Override
    public List<Patients> findAll() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Patients.findAll", Patients.class);
            List<Patients> result = query.getResultList();            
            return result;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Patients findOneByID(int id) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Patients.findByPatientId", Patients.class);
            query.setParameter("patientId", id);
            Patients result = (Patients) query.getSingleResult();
            return result;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Patients> filter(String keyword) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Patients.findAll", Patients.class);
            List<Patients> patients = query.getResultList();

            patients = patients.stream()
                    .filter((item)
                            -> {
                        return item.getFirstName().contains(keyword)
                                || item.getLastName().contains(keyword)
                                || item.getNationalId().contains(keyword);
                    })
                    .collect(Collectors.toList());
            return patients;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long create(Patients patient) {
        try {
            sessionFactory.getObject().getCurrentSession().save(patient);            
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int update(Patients patient) {
        try {
            sessionFactory.getObject().getCurrentSession().update(patient);
            return 1;
        } catch (HibernateException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int delete(Patients patient) {
        return 0;
    }

    @Override
    public Patients findOneByNationalID(String nid) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Patients.findByNationalId", Patients.class);
            query.setParameter("nationalId", nid);
            Patients result = (Patients) query.getSingleResult();
            return result;
        } catch (HibernateException | NoResultException e) {
            e.printStackTrace();
            return null;
        }
    }

}
