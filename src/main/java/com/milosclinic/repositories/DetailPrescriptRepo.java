/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IDetailPrescriptRepo;
import com.milosclinic.models.DetailDrugsPrescriptions;
import com.milosclinic.models.PrescriptionsPK;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ACER
 */
@Repository
@Transactional
public class DetailPrescriptRepo implements IDetailPrescriptRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;
    
    @Override
    public List<DetailDrugsPrescriptions> findByPrescript(PrescriptionsPK prescriptionsPK) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createQuery("SELECT detail FROM DetailDrugsPrescriptions detail WHERE detail.detailDrugsPrescriptionsPK.doctorId = :doctorId AND detail.detailDrugsPrescriptionsPK.patientId = :patientId AND detail.detailDrugsPrescriptionsPK.prescriptId = :prescriptId ORDER BY detail.detailDrugsPrescriptionsPK.detailId DESC");
            query.setParameter("patientId", prescriptionsPK.getPatientId());
            query.setParameter("doctorId", prescriptionsPK.getDoctorId());
            query.setParameter("prescriptId", prescriptionsPK.getPrescriptId());
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public int create(DetailDrugsPrescriptions detail) {
        try {
            sessionFactory.getObject().getCurrentSession().save(detail);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int create(List<DetailDrugsPrescriptions> details) {
        try {
            sessionFactory.getObject().getCurrentSession().save(details);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int update(DetailDrugsPrescriptions detail) {
        try {
            sessionFactory.getObject().getCurrentSession().update(detail);
            return 1;            
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int delete(DetailDrugsPrescriptions detail) {
        try {
            sessionFactory.getObject().getCurrentSession().delete(detail);
            return 1;
        } catch (HibernateException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    
}
