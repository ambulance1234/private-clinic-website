/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IPrescriptRepo;
import com.milosclinic.models.Prescriptions;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Admin
 */
@Repository
@Transactional
public class PrescriptionRepo implements IPrescriptRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;

    @Override
    public List<Prescriptions> findAllByPatient(int patientId) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            var query = session.createQuery("SELECT p FROM Prescriptions p WHERE p.prescriptionsPK.prescriptId = :prescriptId ORDER BY p.prescriptDate DESC", Prescriptions.class);
            query.setParameter("patientId", patientId);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Prescriptions findOneById(int patientId, int doctorId, int prescriptId) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createQuery("SELECT p FROM Prescriptions p WHERE p.prescriptionsPK.doctorId = :doctorId AND p.prescriptionsPK.patientId = :patientId AND p.prescriptionsPK.prescriptId = :prescriptId", Prescriptions.class);
            query.setParameter("patientId", patientId);
            query.setParameter("doctorId", doctorId);
            query.setParameter("prescriptId", prescriptId);
            return (Prescriptions) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long create(Prescriptions newOne) {
        try {
            sessionFactory.getObject().getCurrentSession().save(newOne);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int update(Prescriptions existed) {
        try {
            sessionFactory.getObject().getCurrentSession().update(existed);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int delete(Prescriptions existed) {
        try {
            sessionFactory.getObject().getCurrentSession().save(existed);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public Prescriptions findTheLastOfPatientWithDoctor(int patientId, int doctorId) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createQuery("SELECT p FROM Prescriptions p WHERE p.prescriptionsPK.doctorId = :doctorId AND p.prescriptionsPK.patientId = :patientId ORDER BY p.prescriptionsPK.prescriptId DESC", Prescriptions.class);
            query.setParameter("patientId", patientId);
            query.setParameter("doctorId", doctorId);
            return (Prescriptions) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
