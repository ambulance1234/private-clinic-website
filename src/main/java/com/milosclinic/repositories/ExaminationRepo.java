/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IExaminationRepo;
import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.MedicalExaminationsPK;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Admin
 */
@Repository
@Transactional
public class ExaminationRepo implements IExaminationRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;

    @Override
    public List<MedicalExaminations> findAll() {
        return null;
    }

    @Override
    public List<MedicalExaminations> findOneById(MedicalExaminationsPK target) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Criteria criteria = session.createCriteria(MedicalExaminations.class);
            criteria.add(Restrictions.eq("medicalExaminationsPK.doctorId", target.getDoctorId()));
            criteria.add(Restrictions.eq("medicalExaminationsPK.patientId", target.getPatientId()));
            return criteria.list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long create(MedicalExaminations newOne) {
        try {
            Serializable idSerializable = sessionFactory.getObject()
                    .getCurrentSession().save(newOne);
            Long castToLong = (Long) idSerializable;
            return castToLong;
        } catch (HibernateException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int update(MedicalExaminations existed
    ) {
        try {
            sessionFactory.getObject().getCurrentSession().update(existed);
            return 1;
        } catch (HibernateException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public List<MedicalExaminations> findByPatientId(int patientId
    ) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
//            Query query = session.createNamedQuery("MedicalExaminations.findByPatientId", MedicalExaminations.class);
            Criteria criteria = session.createCriteria(MedicalExaminations.class);
            criteria.add(Restrictions.eq("medicalExaminationsPK.patientId", patientId));
            criteria.addOrder(Order.desc("examDate"));
            var list = criteria.list();
//            query.setParameter("patientId", patientId);
//            var list = query.getResultList();
            return list;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int delete(MedicalExaminations existed) {
        try {
            sessionFactory.getObject().getCurrentSession().delete(existed);
            return 1;
        } catch (HibernateException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

}
