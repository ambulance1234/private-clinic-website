/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IPartRepo;
import com.milosclinic.models.PartsOfDay;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ACER
 */
@Repository
@Transactional
public class PartRepo implements IPartRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;
    
    @Override
    public List<PartsOfDay> findAll() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("PartsOfDay.findAll", PartsOfDay.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public PartsOfDay findOneById(int id) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("PartsOfDay.findByPartId", PartsOfDay.class);
            query.setParameter("partId", id);
            return (PartsOfDay) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
