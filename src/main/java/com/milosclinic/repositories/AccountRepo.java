/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.repositories;

import com.milosclinic.interfaces.repositories.IAccountRepo;
import com.milosclinic.models.Accounts;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Query;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ACER
 */
@Repository
@Transactional
public class AccountRepo implements IAccountRepo {

    @Autowired
    private LocalSessionFactoryBean sessionFactory;

    @Override
    public List<Accounts> findAll() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Accounts.findAll", Accounts.class);
            List<Accounts> result = query.getResultList();
            return result;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Accounts findOneByID(int id) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Accounts.findByAccountId", Accounts.class);
            query.setParameter("accountId", id);
            Accounts result = (Accounts) query.getSingleResult();
            return result;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Accounts findOneByEmail(String email) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Accounts.findByEmail", Accounts.class);
            query.setParameter("email", email);
            Accounts result = (Accounts) query.getSingleResult();
            return result;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Accounts> findAllNurses() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
//            Query query = session.createNamedQuery("Accounts.findAll", Accounts.class);
//            List<Accounts> result = query.getResultList();
            Criteria criteria = session.createCriteria(Accounts.class);
            criteria.createCriteria("roles", "r");
            criteria.add(Restrictions.eq("r.roleId", 3));
            return criteria.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Accounts> findAllDoctors() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
//            Query query = session.createNamedQuery("Accounts.findAll", Accounts.class);
//            List<Accounts> result = query.getResultList();
            Criteria criteria = session.createCriteria(Accounts.class);
            criteria.createCriteria("roles", "r");
            criteria.add(Restrictions.eq("r.roleId", 2));
            return criteria.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Accounts> findAllAdmins() {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
//            Query query = session.createNamedQuery("Accounts.findAll", Accounts.class);
//            List<Accounts> result = query.getResultList();
            Criteria criteria = session.createCriteria(Accounts.class);
            criteria.createCriteria("roles", "r");
            criteria.add(Restrictions.eq("r.roleId", 1));
            return criteria.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param keyword
     * @return
     */
    @Override
    public List<Accounts> filter(String keyword) {
        try {
            Session session = sessionFactory.getObject().getCurrentSession();
            Query query = session.createNamedQuery("Accounts.findAll", Accounts.class);
            List<Accounts> accounts = query.getResultList();

            accounts = accounts.stream()
                    .filter((item)
                            -> {
                        return item.getFirstName().contains(keyword)
                                || item.getLastName().contains(keyword)
                                || item.getPhone().contains(keyword) 
                                || item.getEmail().contains(keyword);
                    })
                    .collect(Collectors.toList());
            return accounts;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean createAccount(Accounts accounts) {
        Session s = this.sessionFactory.getObject().getCurrentSession();
        try {
            s.save(accounts);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
