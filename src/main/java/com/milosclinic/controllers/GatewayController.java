/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author ACER
 */
@Controller
@RequestMapping("/")
public class GatewayController {
    @GetMapping("/404")
    public String notFoundView() {
        return "notfound";
    }
    
//    @GetMapping("/login")
//    public String loginView(Model model) {
////        model.addAttribute("accounts", new Accounts());
//        return "login";
//    }
}
