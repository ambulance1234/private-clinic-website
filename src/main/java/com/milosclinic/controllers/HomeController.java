/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.controllers;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.milosclinic.interfaces.services.IAccountService;
import com.milosclinic.interfaces.services.IDrugService;
import com.milosclinic.interfaces.services.IExaminationService;
import com.milosclinic.interfaces.services.IPatientService;
import com.milosclinic.interfaces.services.IPrescriptionService;
import com.milosclinic.interfaces.services.ITagService;
import com.milosclinic.interfaces.services.IUnitService;
import com.milosclinic.models.Drugs;
import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.Patients;
import com.milosclinic.models.User;
import com.milosclinic.params.Search;
import com.milosclinic.models.Accounts;
import com.milosclinic.services.TagService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author ACER
 */
@Controller
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private IDrugService drugService;

    @Autowired
    private IPatientService patientService;

    @Autowired
    private IExaminationService examService;

    @Autowired
    private IAccountService accountService;
    
    @Autowired
    private ITagService tagService;
    
    @Autowired
    private IUnitService unitService;

    @RequestMapping("/index")
    public String index(Model model) {
        model.addAttribute("name", "Phung Quoc Hung");
        return "index";
    }

    @RequestMapping("/frequency")
    public String frequencyView(Model model) {
        return "frequency";
    }

    @RequestMapping("/report")
    public String reportView(Model model) {
        return "report";
    }

    @GetMapping("/login")
    public String loginView(Model model) {
//        model.addAttribute("accounts", new Accounts());
        return "login";
    }

    private List<Patients> listAllPatients() {
        return patientService.findAll();
    }

    @RequestMapping("/patient")
    public String patientView(Model model) {
        List<Patients> list = patientService.findAll();
        model.addAttribute("patients", list);
        return "patient";
    }

    @GetMapping("/patient/detail/{id}")
    public String detailPatientView(@PathVariable(value = "id") int id, Model model) {

        System.out.println("in patient detail");
        Patients detail = patientService.findOneByID(id);
        var history = examService.findByPatientId(id);
        model.addAttribute("patients", listAllPatients());
        model.addAttribute("patient", detail);
        model.addAttribute("history", history);
        return "patient";
    }

    @GetMapping("/patient/search")
    public String searchPatientsHandler(@RequestParam(value = "keyword") String keyword, Model model) {

        System.out.println(keyword);
        var result = patientService.filter(keyword);
        model.addAttribute("patients", result);
        return "patient";
    }

    @RequestMapping("/drug")
    public String drugView(Model model) {
        var drugs = drugService.findAll();
        model.addAttribute("drugs", drugs);
        return "drug";
    }
    @GetMapping("/drug/detail/{id}")
    public String detailDrugView(@PathVariable(value = "id") int id, Model model) {
        var detail = drugService.findOneByID(id);
        var drugs = drugService.findAll();
        model.addAttribute("drugs", drugs); 
        model.addAttribute("drug", detail);
        return "drug";
    }
    
    @RequestMapping("/addDrug")
    public String addDrugView(Model model) {
        var units = unitService.findAll();
        model.addAttribute("units", units);
        return "addDrug";
    }
    
    @RequestMapping("/editDrug")
    public String editDrugView(@RequestParam(value = "id") int id, Model model) {
        var detail = drugService.findOneByID(id);
        var units = unitService.findAll();
        model.addAttribute("units", units);
        model.addAttribute("detail", detail);
        return "editDrug";
    }

    @RequestMapping("/account/tag={tag}")
    public String accountView(@PathVariable(value = "tag") String tag, Model model) {
        tag = tag.toLowerCase();
        if (tag.equals("nurse")) {
            var list = accountService.findAllNurses();
            model.addAttribute("accounts", list);
            return "account";
        } else if (tag.equals("doctor")) {
            var list = accountService.findAllDoctors();
            model.addAttribute("accounts", list);
            return "account";
        } else if (tag.equals("admin")) {
            var list = accountService.findAllAdmins();
            model.addAttribute("accounts", list);
            return "account";
        } else {
            return "redirect:/404";
        }
    }

    @RequestMapping("/account/detail")
    public String accountDetailHandler(@RequestParam(value = "id") int id, @RequestParam(value = "tag") String tag, Model model) {

        var detail = accountService.findOneByID(id);
        model.addAttribute("detail", detail);

        tag = tag.toLowerCase();
        if (tag.equals("nurse")) {
            var list = accountService.findAllNurses();
            model.addAttribute("accounts", list);
            return "account";
        } else if (tag.equals("doctor")) {
            var list = accountService.findAllDoctors();
            model.addAttribute("accounts", list);
            return "account";
        } else if (tag.equals("admin")) {
            var list = accountService.findAllAdmins();
            model.addAttribute("accounts", list);
            return "account";
        } else {
            return "redirect:/404";
        }
    }

    @GetMapping("/account/search")
    public String searchAccountsHandler(@RequestParam(value = "keyword") String keyword, Model model) {

        System.out.println(keyword);
        var result = accountService.filter(keyword);
        model.addAttribute("accounts", result);
        return "account";
    }

    @RequestMapping("/prescriptions")
    public String presciptionsView(Model model) {
        return "prescriptions";
    }
    
    @RequestMapping("/pres2")
    public String pres2View(Model model) {
        return "pres2";
    }

    @RequestMapping("/addNewAccount")
    public String addNewAccountView(Model model) {
        return "addNewAccount";
    }

    // @RequestMapping("/account/editAccount")
    // public String editAccountView(Model model) {
    //     return "editAccount";
    // }
    
    @GetMapping("/account/editAccount/{id}")
    public String editAccountView(@PathVariable(value = "id") int id, Model model) {
        System.out.println(id);
        if (id == 0) {
            return "redirect:/404";
        }

        var account = accountService.findOneByID(id);
        model.addAttribute("account", account);
        return "editAccount";
    }

    @GetMapping("/patient/addNewPatient")
    public String addNewPatientView(Model model) {
        return "addNewPatient";
    }

//    @RequestMapping("/editPatient")
//    public String editPatientView(Model model) {
//        return "editPatient";
//    }

    @RequestMapping("/examForm")
    public String examFormView(Model model) {
        var doctorList = accountService.findAllDoctors();
        var patientList = patientService.findAll();
       model.addAttribute("doctors", doctorList);
       model.addAttribute("patients", patientList);
        return "examForm";
    }

    @GetMapping("/register")
    public String reigsterView(Model model) {
        var tag = tagService.findAll();
        model.addAttribute("tags", tag);
        model.addAttribute("accounts", new Accounts());
        return "register";
    }

    @PostMapping("/register")
    public String reigsterHandler(Model model, @ModelAttribute(value = "accounts") @Valid Accounts accounts, BindingResult result) {
//        System.out.println(accounts.getEmail() + " " + accounts.getPassword() + " " + accounts.getPhone() + " " + accounts.getGenderId() + " " + accounts.getTagId() );

        System.out.println(accounts.toString());

if (result.hasErrors() == true) {
            return "register";
        }

        String errMsg;
        if (accounts.getPassword().equals(accounts.getConfirmPassword())) {
            if (this.accountService.createAccount(accounts) == true) {
                return "redirect:/home/login";
            } else {
                errMsg = "Something wrong!! Please try again later!";
            }
        } else {
            errMsg = "Password does not match!";
        }
        model.addAttribute("errMsg", errMsg);
        return "register";
    }

    @GetMapping("/patient/edit/{id}")
    public String editPatientView(@PathVariable(value = "id") int id, Model model) {
        System.out.println(id);
        if (id == 0) {
            return "redirect:/404";
        }

        var patient = patientService.findOneByID(id);
        model.addAttribute("patient", patient);
        return "editPatient";
    }

    @PostMapping("/edit")
    public String editPatientHandler(@ModelAttribute("requestPatient") Patients patient, Model model) {
        var reuslt = patientService.update(patient);
        var updated = patientService.findOneByID(patient.getPatientId());
        model.addAttribute("patient", updated);
        return "editPatient";
    }

    @Autowired
    private Cloudinary cloudinary;

    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public String upload(@ModelAttribute("user") User user) {
        try {
            Map response = cloudinary.uploader().upload(user.getAvatar().getBytes(),
                    ObjectUtils.asMap("resource_type", "image"));
            System.out.println(response);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return "index";
    }
}
