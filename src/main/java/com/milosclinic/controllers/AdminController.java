/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.controllers;

import com.milosclinic.interfaces.services.IAccountService;
import com.milosclinic.interfaces.services.IDrugService;
import com.milosclinic.interfaces.services.IExaminationService;
import com.milosclinic.models.Accounts;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Admin
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

//    @Autowired
//    private IExaminationService examinationService;
//    
//    @Autowired
//    private IDrugService drugService;
//    
//    @Autowired
//    private IAccountService accountService;

    @RequestMapping("/report")
    public String index(HttpServletRequest request) {
        Accounts account = (Accounts) request.getSession().getAttribute("currentUser");
        if (account.getRoles().getRoleName().equals("ADMIN") == false) {
            return "redirect:/home/login";
        }
        return "report";
    }
}
