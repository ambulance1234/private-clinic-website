/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.controllers;

import com.milosclinic.interfaces.services.IDrugService;
import com.milosclinic.interfaces.services.IPrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Admin
 */
@Controller
@RequestMapping("/prescript")
public class PrescriptController {

    @Autowired
    private IPrescriptionService prescriptService;
    
    @Autowired
    private IDrugService drugService;

    @RequestMapping("/")
    public String index() {
        return "";
    }
}
