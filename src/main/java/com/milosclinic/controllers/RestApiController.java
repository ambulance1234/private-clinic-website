/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.controllers;

import com.milosclinic.dto.PatientCreateDto;
import com.milosclinic.dto.PatientUpdateDto;
import com.milosclinic.interfaces.services.IDetailService;
import com.milosclinic.interfaces.services.IDrugService;
import com.milosclinic.interfaces.services.IExaminationService;
import com.milosclinic.interfaces.services.IPatientService;
import com.milosclinic.interfaces.services.IPrescriptionService;
import com.milosclinic.models.Accounts;
import com.milosclinic.models.DetailDrugsPrescriptions;
import com.milosclinic.models.Drugs;
import com.milosclinic.models.Genders;
import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.MedicalExaminationsPK;
import com.milosclinic.models.Patients;
import com.milosclinic.models.Prescriptions;
import com.milosclinic.parser.AutoMapping;
import com.milosclinic.parser.RequestParser;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ACER
 */
@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestApiController {

    @Autowired
    private IExaminationService examinationService;

    @Autowired
    private IPatientService patientService;

    @Autowired
    private IDrugService drugService;
    
    @Autowired
    private IPrescriptionService prescriptionService;
    
    @Autowired
    private IDetailService detailService;

//    @DeleteMapping("/exam/remove")
    @RequestMapping(
            value = "/exam/remove",
            method = RequestMethod.PUT,
            consumes = "application/json",
            headers = "content-type=application/json")
    public ResponseEntity deleteExamHandler(HttpServletRequest request) {

        System.out.println("remove exam");

        var examPK = RequestParser.requestBodyHandler(request, MedicalExaminationsPK.class);

        System.out.println(examPK.getExamId() + " " + examPK.getDoctorId() + " " + examPK.getPatientId());
        var exam = new MedicalExaminations();
        exam.setMedicalExaminationsPK(examPK);
        int deleted = examinationService.delete(exam);
//        int deleted = 1;
        if (deleted == 0) {
            var message = new HashMap<String, String>();
            message.put("message", "Fail to remove");

            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        var message = new HashMap<String, String>();
        message.put("message", "Completed remove");
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/exam/create")
    public ResponseEntity createExam(HttpServletRequest request) {
        var exam = RequestParser.requestBodyHandler(request, MedicalExaminations.class);
        long created = examinationService.create(exam);
        if (created > 0) {
            return ResponseEntity.ok("Created");
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/patient/create")
    public ResponseEntity addNewPatientHandler(HttpServletRequest request) {
        var patientDto = RequestParser.requestBodyHandler(request, PatientCreateDto.class);
        System.out.println(patientDto.toString());
        Patients patient = AutoMapping.mapTo(PatientCreateDto.class, patientDto, Patients.class, Patients::new);
        System.out.println(patient.toString());

        Genders gender = new Genders();
        gender.setGenderName(patientDto.getGender());
        patient.setGenderId(gender);

        long created = patientService.create(patient);

        if (created > 0) {
            return ResponseEntity.ok("created");
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/patient/edit")
    public ResponseEntity editPatientHandler(HttpServletRequest request) {
        var patientDto = RequestParser.requestBodyHandler(request, PatientUpdateDto.class);
        Patients patient = AutoMapping.mapTo(PatientUpdateDto.class, patientDto, Patients.class, Patients::new);
        System.out.println(patient.toString());

        Genders gender = new Genders();
        gender.setGenderName(patientDto.getGender());
        patient.setGenderId(gender);

        long created = patientService.update(patient);

        if (created > 0) {
            return ResponseEntity.ok("updated");
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/drugs/create")
    public ResponseEntity createDrugHandler(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Accounts account = (Accounts) session.getAttribute("currentUser");
        
        if(account == null ) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        
        if (account.getRoles().getRoleId() != 1) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        
        Drugs drug = RequestParser.requestBodyHandler(request, Drugs.class);
        System.out.println(drug.toString());
        int created = drugService.create(drug);
        if (created == 1) {
            return ResponseEntity.ok("Created");
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/drugs/edit/{id}")
    public ResponseEntity editDrugHandler(HttpServletRequest request) {
        Drugs drug = RequestParser.requestBodyHandler(request, Drugs.class);
        int updated = drugService.update(drug);
        if (updated == 1) {
            return ResponseEntity.ok("updated");
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/drugs/remove")
    public ResponseEntity removeDrugHandler(HttpServletRequest request) {
        Drugs drug = RequestParser.requestBodyHandler(request, Drugs.class);
        int deteled = drugService.delete(drug);
        if (deteled == 1) {
            return ResponseEntity.ok("removed");
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
    
    @PostMapping("/prescriptions/create")
    public ResponseEntity createPrescriptHandler(HttpServletRequest request) {
        Prescriptions model = RequestParser.requestBodyHandler(request, Prescriptions.class);
        long created = prescriptionService.create(model);
        if(created == 1) {
            return ResponseEntity.ok("created");
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    
    @PutMapping("/prescriptions/edit")
    public ResponseEntity editPrescriptHandler(HttpServletRequest request) {
        Prescriptions model = RequestParser.requestBodyHandler(request, Prescriptions.class);
        int updated = prescriptionService.update(model);
        if(updated == 1) {
            return ResponseEntity.ok("updated");
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("/prescriptions/remove")
    public ResponseEntity deletePrescriptHandler(HttpServletRequest request) {
        Prescriptions model = RequestParser.requestBodyHandler(request, Prescriptions.class);
        int deleted = prescriptionService.delete(model);
        if(deleted == 1) {
            return ResponseEntity.ok("deleted");
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    
    @PostMapping("/prescriptions/detail/create")
    public ResponseEntity createDetailPrescriptHandler(HttpServletRequest request) {
        DetailDrugsPrescriptions model = RequestParser.requestBodyHandler(request, DetailDrugsPrescriptions.class);
        int created = detailService.create(model);
        if(created == 1) {
            return ResponseEntity.ok("created");
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    
    @PostMapping("/prescriptions/detail/edit")
    public ResponseEntity editDetailPrescriptHandler(HttpServletRequest request) {
        DetailDrugsPrescriptions model = RequestParser.requestBodyHandler(request, DetailDrugsPrescriptions.class);
        int updated = detailService.update(model);
        if(updated == 1) {
            return ResponseEntity.ok("updated");
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("/prescriptions/detail/remove")
    public ResponseEntity deleteDetailPrescriptHandler(HttpServletRequest request) {
        DetailDrugsPrescriptions model = RequestParser.requestBodyHandler(request, DetailDrugsPrescriptions.class);
        int deleted = detailService.delete(model);
        if(deleted == 1) {
            return ResponseEntity.ok("deleted");
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
