/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.controllers;

import com.milosclinic.interfaces.services.IDrugService;
import com.milosclinic.interfaces.services.IPatientService;
import com.milosclinic.models.Drugs;
import com.milosclinic.models.Patients;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ACER
 */
@RestController
@RequestMapping("/api/test")
public class TestController {
    
    @Autowired
    private IDrugService drugService;
    
    @Autowired
    private IPatientService patientService;
    
    @GetMapping("/drugs")
    public String getDrugsList() {
        List<Drugs> drugsList = drugService.findAll();
        String responseString = "";
        for (Drugs drugs : drugsList) {
            responseString += drugs.toString() + "\n";
        }
        return responseString;
    }
    
    @GetMapping("/patients")
    public String getPatientByID(@RequestParam(value = "id") int id) {
        Patients patient = patientService.findOneByID(id);
        return patient.getFirstName();
    }
    @GetMapping("/patients/search")
    public String searchPatients(@RequestParam(value = "q") String keyword) {
        var patient = patientService.filter(keyword);
        String result = "";
        patient.forEach(item -> System.out.println(item.getFirstName() + " " + item.getLastName()));
        return result;
    }
}
