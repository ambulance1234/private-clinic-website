/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.milosclinic.controllers;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author ACER
 */
@Controller
@RequestMapping("/notice")
public class NoticeController {

    @GetMapping("/success")
    public String successful() {
        return "successful";
    }
    @GetMapping("/fail")
    public String fail() {
        return "failure";
    }
    @GetMapping("/decision")
    public String decision() {
        return "decision";
    }
}
