<%-- Document : patient Created on : Apr 6, 2022, 2:52:48 PM Author : Admin --%>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link href="<c:url value = "/css/fixedTable.css" />" rel="stylesheet" />

<div style="width: 100%; height: 80vh">
    <!-- body -->
    <div class="container-fluid" style="height: 80%">
        <div class="row">
            <div class="col-sm-6">
                <c:url value="/home/patient/search" var="searchUrl" />
                <form class="input-group mb-3" method="get" action="${searchUrl}" >
                    <input type="text" class="form-control" placeholder="Search Patient..." name="keyword"/>

                    <div class="input-group-append">
                        <button class="btn btn-primary" style="color: white" type="submit" >Search</button>
                    </div>
                </form>
                <div class="fixTableHead">
                    <table
                        class="table table-hover"
                        style="color: white; text-align: center"
                        >
                        <thead style="background: #0804ab">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>H</th>
                                <th>W</th>
                                <th>Age</th>
                                <th>Sex</th>
                                <th>Created at</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody style="background: #585798">
                            <c:forEach items="${patients}" var="p">
                                <%--<c:url value="" var="detail" />--%>
                                <tr>
                                    <c:url
                                        value="/home/patient/detail/${p.patientId}"
                                        var="detail"
                                        />
                                    <td>${p.patientId}</td>
                                    <td>${p.firstName}</td>
                                    <td>${p.height}</td>
                                    <td>${p.weight}</td>
                                    <td>${p.age}</td>
                                    <td>${p.genderId.genderName}</td>
                                    <td>${p.createdAt.toString().split(" ")[0]}</td>
                                    <td>
                                        <a href="${detail}" style="color: white">
                                            <span class="material-icons"> info </span>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-6" style="padding: 0">
                <div style="height: 280px">
                    <div class="d-flex flex-row-reverse" style="background: #ffecec">
                        <div class="p-2 bg-info">Detail Information</div>
                        <button class="p-2 bg-warning">Prescriptions</button>
                    </div>
                    <div style="display: flex; justify-content: space-around">
                        <div class="form-group" style="width: 60px">
                            <label for="usr">ID</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.patientId}"
                                />
                        </div>
                        <div class="form-group" style="width: 170px">
                            <label for="usr">First Name</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.firstName}"
                                />
                        </div>
                        <div class="form-group" style="width: 170px">
                            <label for="usr">Last Name</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.lastName}"
                                />
                        </div>
                        <div class="form-group" style="width: 65px">
                            <label for="usr">Height</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.height}"
                                />
                        </div>
                        <div class="form-group" style="width: 60px">
                            <label for="usr">Weight</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.weight}"
                                />
                        </div>
                        <div class="form-group" style="width: 60px">
                            <label for="usr">Age</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.age}"
                                />
                        </div>
                        <div class="form-group" style="width: 60px">
                            <label for="usr">Sex</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.genderId.genderName}"
                                />
                        </div>
                    </div>
                    <div
                        style="display: flex; justify-content: space-around; width: 100%"
                        >
                        <div class="form-group" style="width: 50%">
                            <label for="comment">Address</label>
                            <input
                                class="form-control"
                                disabled
                                style="background: #dfdfdf"
                                value="${patient.address}"
                                />
                            <label for="usr">National ID:</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.nationalId}"
                                />
                        </div>
                        <div class="form-group" style="width: 45%">
                            <label for="usr">Created at</label>
                            <input
                                type="text"
                                disabled
                                class="form-control"
                                style="background: #dfdfdf"
                                value="${patient.createdAt}"
                                />
                            <div
                                style="height: 50%; align-items: end"
                                class="d-flex flex-row-reverse"
                                >
                                <c:url value="/home/patient" var="link" />
                                <c:if test="${patient.patientId == null}">
                                    <a href="${link}/edit/0" class="btn" style="background: #00970F; color: white; width: 40%">Edit</a>
                                </c:if>
                                <c:if test="${patient.patientId != null}">
                                    <a href="${link}/edit/${patient.patientId}" class="btn" style="background: #00970F; color: white; width: 40%">Edit</a>
                                </c:if>

                                    <a href="<c:url value="/home/register" />" class="btn" style="background: #970091; color: white; width: 40%; margin-right: 10% ">New</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div
                        class="d-flex flex-row-reverse"
                        style="background: #ffecec; color: white"
                        >
                        <div class="p-2" style="background: #36a1cf">History Exams</div>
                        <button class="p-2" style="background: #3645cf; color: white">
                            New Exam
                        </button>
                    </div>

                    <c:if test="${history.isEmpty() == true}" >
                        <div>
                            NO HISTORY OF MEDICAL EXAMINATION
                        </div>
                    </c:if>
                    <c:if test="${history.isEmpty() == false}">
                        <div class="fixTableHead" style="height: 225px">
                            <table class="table table-hover" style="color: white">
                                <thead style="background: #005275">
                                    <tr>
                                        <th>Doctor</th>
                                        <th>Date</th>
                                        <th>Reason</th>
                                        <th><span class="material-icons">
                                                delete
                                            </span></th>
                                    </tr>
                                </thead>
                                <tbody style="background: #648594">
                                    <c:forEach items="${history}" var="h" >
                                        <tr>
                                            <td>${h.accounts.firstName} ${h.accounts.lastName}</td>
                                            <td>
                                                <jsp:useBean id="today" class="java.util.Date" />
                                                <c:if test="${h.examDate.time lt today.time}">
                                                    <div style="background-color: tomato">${h.examDate}</div>
                                                </c:if>
                                                <c:if test="${h.examDate.time gt today.time}">
                                                    <div style="background-color: #B6FF58">${h.examDate}</div>
                                                </c:if>
                                                <c:if test="${h.examDate.time eq today.time}">
                                                    <div style="background-color: #FCFF58">${h.examDate}</div>
                                                </c:if>
                                            </td>
                                            <td>${h.reason}</td>
                                            <td>
                                                <span class="material-icons" style="cursor: pointer;" onclick="cancelExam(
                                                      ${h.medicalExaminationsPK.examId},
                                                      ${h.medicalExaminationsPK.doctorId},
                                                      ${h.medicalExaminationsPK.patientId}
                                                        )">
                                                    delete_outline
                                                </span> 
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </c:if>
                </div> 
            </div>
        </div>
    </div>
</div>
<script>
    async function cancelExam(examId, doctorId, patientId) {
        const choice = confirm("Do you want to delete this exam? Can not undo the action!");

        if (choice === false) {
            return alert("You canceled the remove action");
        }
        const exam = {
            examId,
            doctorId,
            patientId
        };

        try {
            const res = await fetchApi(exam);
            console.log(res);
            console.log(res.status);
            if (res.status === 200) {
                alert("Completely removed the exam!");
                window.location.reload();
            } else {
                alert("Can not remove the exam");
            }
        } catch (e) {
            console.log("error: ");
            console.log(e);
        }
    }


    async function fetchApi(exam) {
        return fetch('http://localhost:9090/PrivateClinicProjekt/api/exam/remove',
                {
                    method: 'PUT',
                    mode: 'cors',
                    cache: 'no-cache',
                    credentials: 'same-origin',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    body: JSON.stringify(exam)
                });
    }
</script>

