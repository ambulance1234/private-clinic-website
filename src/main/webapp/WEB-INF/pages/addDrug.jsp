<%-- 
    Document   : addDrug
    Created on : Apr 15, 2022, 10:12:25 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <link href="<c:url value = "/css/drug_detail.css" />" rel="stylesheet" />
    </head>
    <body>
        <div style="height: 100vh; display: flex; align-items: center; justify-content: center">
            <div class="col-sm-6 drug_detail">
                <p2 class="drug_detail_title">Add Drug</p2>
                <form class="drug_detail_form">
                    <label>Code</label>
                    <input id="drugcode" />    
                </form>
                <form class="drug_detail_form">
                    <label>Drug name</label>
                    <input id="drugname"/>    
                </form>
                <form class="drug_detail_form">
                    <label>Company</label>
                    <input id="company"/>
                    <a href="../../../java/com/milosclinic/controllers/HomeController.java"></a>
                </form>
                <form class="drug_detail_form">
                    <label>Origin</label>
                    <input id="origin"/>
                </form>
                <form class="drug_detail_form">
                    <label>Price</label>
                    <input id="price"/>
                </form>
                <form class="drug_detail_form">
                    <label>Unit</label>
                    <select id="unit">
                        <c:forEach items="${units}" var="u" >
                            <option value="${u.unitId}:${u.unitName}">${u.unitName}</option>
                        </c:forEach>
                    </select>

                </form>
                <div class="drug_detail_btn">                   
                    <button style="background: #2ecc71" onclick="createNewDrug()">Add</button>
                    <button onclick="backToDrug()" style="background: #e74c3c">Cancel</button>
                </div>
            </div>
        </div>
        <script>
            async function createNewDrug() {
                const registedCode = document.getElementById("drugcode").value;
                const drugName = document.getElementById("drugname").value;
                const company = document.getElementById("company").value;
                const origin = document.getElementById("origin").value;
                const price = document.getElementById("price").value;
                const unitSelect = document.getElementById("unit").value;

                const unit = {
                    unitId: unitSelect.split(":")[0],
                    unitName: unitSelect.split(":")[1]
                };

//                console.log(unit);
                const body = {
                    registedCode,
                    drugName,
                    company,
                    origin,
                    price,
                    unit
                };
                
                try {
                    const res = await createDrugApi(body);
                    console.log(res);

                    if (res.status === 200) {
                        alert("Completely created the new drug!");
                        window.location.href = "http://localhost:9090/PrivateClinicProjekt/home/drug";
                    } else {
                        alert("Can not create the drug");
                        window.location.reload();
                    }
                } catch (e) {
                    console.log(e);
                    alert("Some errors occured in creating new drug process");
                    window.location.reload();
                }
            }

            async function createDrugApi(body) {
                return fetch('http://localhost:9090/PrivateClinicProjekt/api/drugs/create',
                        {
                            method: 'POST',
                            mode: 'cors',
                            cache: 'no-cache',
                            credentials: 'same-origin',
                            headers: {
                                'Content-Type': 'application/json;charset=UTF-8'
                            },
                            body: JSON.stringify(patient)
                        });
            }

            function backToDrug() {
                window.history.go(-1);
                return false;
            }
        </script>
    </body>
</html>
