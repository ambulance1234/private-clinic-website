<%-- 
    Document   : doctor
    Created on : Apr 6, 2022, 2:53:57 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!-- body -->
<div class="container-fluid" style="height: 80%;">
    <div class="row">
        <div class="col-sm-6">
            <c:url value="/home/account/search" var="searchUrl" />
            <form class="input-group mb-3" action="${searchUrl}">
                <input type="text" class="form-control" placeholder="Search Patient..." name="keyword">
                <div class="input-group-append">
                    <button class="btn btn-primary" style="color: white" type="submit">Search</button>  
                </div>
            </form>
            <div class="fixTableHead">
                <table class="table table-hover" style="color: white; text-align: center">
                    <thead style="background: #0804AB;">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Tag</th>
                            <th>Age</th>
                            <th>Sex</th>
                            <th>Created at</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody style="background: #585798">
                        <c:forEach items="${accounts}" var="a">

                            <tr>
                                <c:url value="/home/account/detail?id=${a.accountId}&tag=${a.roles.roleName}" var="detailInfo" />
                                <td>${a.accountId}</td>
                                <td>${a.firstName} </td>
                                <td>${a.tagId.name}</td>
                                <td>${a.age} </td>
                                <td>${a.genderId.genderName}</td>
                                <td>${a.createdAt}</td>
                                <td>
                                    <a href="${detailInfo}" style="color: white">
                                        <span class="material-icons">
                                            info
                                        </span>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-sm-6" style=" padding:0">
            <div style="height: 280px; ">
                <div class="d-flex flex-row-reverse" style="background: #FFECEC">
                    <div class="p-2 bg-info">Detail Information</div>
                    
                </div>
                <div style="display: flex; justify-content: space-around; ">
                    <div class="form-group" style="width: 60px">
                        <label for="usr">ID</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.accountId}">
                    </div>
                    <div class="form-group" style="width: 170px">
                        <label for="usr">First Name</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.firstName}">
                    </div>
                    <div class="form-group" style="width: 170px">
                        <label for="usr">Last Name</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.lastName}">
                    </div>
                    <div class="form-group" style="width: 140px">
                        <label for="usr">Tag</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.tagId.name}">
                    </div>
                    <div class="form-group" style="width: 60px">
                        <label for="usr">Age</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.age}">
                    </div>
                    <div class="form-group" style="width: 80px">
                        <label for="usr">Sex</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.genderId.genderName}">
                    </div>
                </div>
                <div style="display: flex; justify-content: space-around; width: 100%;">
                    <div class="form-group" style="width: 50%">
                        <label for="comment">Email</label>
                        <input type="text" class="form-control" disabled style="background: #DFDFDF;" value="${detail.email}"></input>
                        <label for="usr">Phone</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.phone}">
                    </div>
                    <div class="form-group" style="width: 45%">
                        <label for="usr">Created at</label>
                        <input type="text" disabled class="form-control" style="background: #DFDFDF" value="${detail.createdAt}">
                        <div style="height: 50%; align-items: end; " class="d-flex flex-row-reverse">
                            <button class="btn" style="background: #00970F; color: white; width: 40%">Edit</button>
                            <button class="btn" style="background: #970091; color: white; width: 40%; margin-right: 10% ">New</button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: auto; display: flex; align-items: center; justify-content: center; flex-direction: column">
                <c:url value="https://media.istockphoto.com/vectors/doctor-icon-or-avatar-physician-with-stethoscope-medicine-symbol-vector-id1150502263?k=20&m=1150502263&s=170667a&w=0&h=4MSO2AP7370aLjFd1tXdsf0FoJMRshIsgRS_5HpZ1E4=" var="defaultImg"/>
                <c:url value="${detail.imageSource}" var="accImg" />
                <c:if test="${accImg.isEmpty() == true}">
                    <div style="width: 250px; height: 250px;">
                        <img style="width: 100%; height: 100%;object-fit: contain" src="${defaultImg}" />
                    </div>
                </c:if>
                <c:if test="${accImg.isEmpty() != true}">
                    <div style="width: 250px; height: 250px;">
                        <img style="width: 100%; height: 100%;object-fit: contain" src="${accImg}" />
                    </div>
                    
                </c:if>
                <div>
                    Employee's avatar
                </div>
            </div>

        </div>

    </div>
</div>
</div>
</div>
