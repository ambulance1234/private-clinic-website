<%-- 
    Document   : addNew
    Created on : Apr 9, 2022, 8:52:33 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
            <!-- body -->
            <div class="container-fluid" style="height: 80%;">
                <div style="width: 100%; display: flex; justify-content: center; align-items: center">
                    <div class="col-sm-6" style=" padding:0">
                        <div style="height: 200px; ">

                    <div style="display: flex; justify-content: space-around; ">
                        <div class="form-group" style="width: 60px">
                            <label for="usr">ID</label>
                            <input type="text" disabled class="form-control" style="background: #DFDFDF">
                        </div>
                        <div class="form-group" style="width: 170px">
                            <label for="usr">First Name</label>
                            <input type="text" disabled class="form-control" style="background: #DFDFDF">
                        </div>
                        <div class="form-group" style="width: 170px">
                            <label for="usr">Last Name</label>
                            <input type="text" disabled class="form-control" style="background: #DFDFDF">
                        </div>
                        <div class="form-group" style="width: 120px">
                            <label for="usr">Tag</label>
                            <input type="text" disabled class="form-control" style="background: #DFDFDF">
                        </div>                           
                        <div class="form-group" style="width: 60px">
                            <label for="usr">Age</label>
                            <input type="text" disabled class="form-control" style="background: #DFDFDF">
                        </div>
                        <div class="form-group" style="width: 60px">
                            <label for="usr">Sex</label>
                            <input type="text" disabled class="form-control" style="background: #DFDFDF">
                        </div>
                    </div>
                    <div style="display: flex; justify-content: space-around; width: 100%;">
                        <div class="form-group" style="width: 50%">
                            <label for="comment">Email</label>
                            <textarea class="form-control" style="background: #DFDFDF; resize: none" rows="1"></textarea>
                            <label for="usr">Phone</label>
                            <input type="text" class="form-control" style="background: #DFDFDF">
                        </div>
                        <div class="form-group" style="width: 45%">
                            <label for="usr">Created at</label>
                            <input type="text" class="form-control" style="background: #DFDFDF">
                            <div style="height: 50%; align-items: end; " class="d-flex flex-row-reverse">
                                <button class="btn" style="background: #00639B; color: white; width: 40%">Save</button>
                                <button class="btn" style="background: #970000; color: white; width: 40%; margin-right: 10% ">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="height: auto; display: flex; align-items: center; justify-content: center; flex-direction: column">
                    <form>
                        <input type="file" id="myFile" name="filename" accept="image/*" class="regist_chooseimg">                   
                    </form>
                </div> 
            </div>
        </div>
    </div>
</div>
