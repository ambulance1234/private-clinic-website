<%-- 
    Document   : notfound
    Created on : Apr 13, 2022, 12:40:48 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>404 Page</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <div class="flex-column" style="width: 100%; height: 100vh; display: flex; justify-content: center; align-items: center;">
            <img class="rounded-circle" style="width: 200px; height: 200px" src="https://i.redd.it/pubbsk4oked41.jpg" />
            <div style="display: flex; justify-content: space-between; align-items: center; height: 100px; font-size: 30px;">
                <span class="material-icons" style="font-size: 100px; color: tomato">
                    directions_off
                </span>
                <div style="width: 20px"></div>
                <div style=" color: tomato; font-weight: bold">
                    <div>NOT FOUND PAGE</div>
                    <a href="http://localhost:9090/PrivateClinicProjekt/home/patient" type="button" class="btn btn-danger" style="width: 100%" >Go back</a></div>
            </div>            
        </div>
    </body>
</html>
