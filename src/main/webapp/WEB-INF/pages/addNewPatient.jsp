<%-- 
    Document   : addNewPatient
    Created on : Apr 10, 2022, 11:08:12 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Calendar"%>
        <%--<%@page import="java.util.GregorianCalendar"%>--%>
    </head>
    <body>
        <div style="width: 100%; height: 100vh;">
            <!-- Header  -->
            <div class="container-fluid">
                <div class="row" style="background: #003079; color: white; font-size: 20px" >
                    <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center">Milos Clinic</div>
                    <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center" >
                        <img src="https://freetuts.net/upload/product_series/images/2021/11/29/2446/ricardo-milos.JPG" style="width: 80px; height: 80px; border-radius: 50%;"/>
                    </div>
                    <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center">
                        Dr.Ricardo
                        <img src="https://freetuts.net/upload/product_series/images/2021/11/29/2446/ricardo-milos.JPG" style="width: 70px; height: 70px; border-radius: 50%; margin-left: 2%"/>
                    </div>
                </div>
            </div>
            <!-- 4 tags bên trái -->
            <div>
                <div class="d-flex justify-content-start ">
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FF5858">Patient</button>
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FFBC58">Drug</button>
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FCFF58">Nurse</button>
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #B6FF58">Doctor</button>
                </div>
            </div>
            <!-- body -->
            <div class="container-fluid" style="height: 80%;">
                <div style="width: 100%; display: flex; justify-content: center; align-items: center">
                    <div class="col-sm-6" style=" padding:0">
                        <div style="height: 250px;">

                            <div style="display: flex; justify-content: space-around;">
                                <div class="form-group" style="width: 60px">
                                    <label for="usr">ID</label>
                                    <input type="text" disabled class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 170px">
                                    <label for="usr">First Name</label>
                                    <input id="fname" type="text" class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 170px">
                                    <label for="usr">Last Name</label>
                                    <input id="lname" type="text" class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 65px">
                                    <label for="usr">Height</label>
                                    <input id="height" type="text" class="form-control" style="background: #DFDFDF" value="${patient.height}" >
                                </div>
                                <div class="form-group" style="width: 60px">
                                    <label for="usr">Weight</label>
                                    <input id="weight" type="text" class="form-control" style="background: #DFDFDF" value="${patient.weight}" >
                                </div>                          
                                <div class="form-group" style="width: 60px">
                                    <label for="usr">Age</label>
                                    <input id="age" type="text" class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 60px">
                                    <label for="usr">Sex</label>
                                    <input id="gender" type="text" class="form-control" style="background: #DFDFDF">
                                </div>
                            </div>
                            <div style="display: flex; justify-content: space-around; width: 100%;">
                                <div class="form-group" style="width: 50%">
                                    <label for="comment">Address</label>
                                    <textarea id="addrs" class="form-control" style="background: #DFDFDF; resize: none" rows="1"></textarea>
                                    <label for="usr">National ID</label>
                                    <input id="nid" type="text" class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 45%">
                                    <jsp:useBean id="today" class="java.util.Date" />
                                    <label for="usr">Created at</label>
                                    <input type="text" class="form-control" disabled style="background: #DFDFDF" value="${today.toLocaleString()}">
                                    <div style="height: 50%; align-items: end; " class="d-flex flex-row-reverse">
                                        <button class="btn" style="background: #00639B; color: white; width: 40%" type="submit" onclick="createNewPatient()" >Save</button>
                                        <button class="btn" style="background: #970000; color: white; width: 40%; margin-right: 10%;" onclick="goBackToPatients()">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="height: auto; display: flex; align-items: center; justify-content: center">

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script>
            async function createNewPatient() {

                const firstName = document.getElementById("fname").value;
                const lastName = document.getElementById("lname").value;
                const height = document.getElementById("height").value;
                const weight = document.getElementById("weight").value;
                const age = document.getElementById("age").value;
                const gender = document.getElementById("gender").value;
                const address = document.getElementById("addrs").value;
                const nationalId = document.getElementById("nid").value;

                const body = {
                    firstName,
                    lastName,
                    height,
                    weight,
                    age,
                    gender,
                    address,
                    nationalId
                };

                console.log(body);

                try {
                    const res = await fetchApi(body);
                    console.log(res);

                    if (res.status === 200) {
                        alert("Completely created the new patient!");
                        window.location.href = "http://localhost:9090/PrivateClinicProjekt/home/patient";
                    } else {
                        alert("Can not create the patient");
                        window.location.reload();
                    }
                } catch (e) {
                    console.log(e);
                    alert("Some errors occured in creating new patient process");
                    window.location.reload();
                }


//                return false;
            }
            async function fetchApi(patient) {
                return fetch('http://localhost:9090/PrivateClinicProjekt/api/patient/create',
                        {
                            method: 'POST',
                            mode: 'cors',
                            cache: 'no-cache',
                            credentials: 'same-origin',
                            headers: {
                                'Content-Type': 'application/json;charset=UTF-8'
                            },
                            body: JSON.stringify(patient)
                        });
            }
            
            function goBackToPatients() {
                window.location.href = "http://localhost:9090/PrivateClinicProjekt/home/patient";
            }
        </script>
    </body>
</html>
