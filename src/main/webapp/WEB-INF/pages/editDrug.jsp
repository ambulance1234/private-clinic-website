<%-- 
    Document   : editDrug
    Created on : Apr 15, 2022, 10:12:37 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <link href="<c:url value = "/css/drug_detail.css" />" rel="stylesheet" />
    </head>
    <body>
        <div style="height: 100vh; display: flex; align-items: center; justify-content: center">
            <div class="col-sm-6 drug_detail">
                <p2 class="drug_detail_title">Edit Drug</p2>
                <form class="drug_detail_form">
                    <label>Code</label>
                    <input id="registedCode" value="${detail.registedCode}"/>    
                </form>
                <form class="drug_detail_form">
                    <label>Drug name</label>
                    <input id="drugname" value="${detail.drugName}" />    
                </form>
                <form class="drug_detail_form">
                    <label>Company</label>
                    <input id="company" value="${detail.company}"/>
                </form>
                <form class="drug_detail_form">
                    <label>Origin</label>
                    <input id="origin" value="${detail.origin}"/>
                </form>
                <form class="drug_detail_form">
                    <label>Price</label>
                    <input id="price" value="${detail.price}"/>
                </form>
                <form class="drug_detail_form"style="height: 100px;">
                    <label>Unit</label>
                    <div  style="width: 50%; height: 50%">
                        <div>
                            Present: ${detail.unit.unitName}
                        </div>
                        <select id="unit"style="width: 100%; height: 100%; ">
                            <c:forEach items="${units}" var="u">
                                <option value="${u.unitId}:${u.unitName}">${u.unitName}</option>
                            </c:forEach> 
                        </select>
                    </div>
                </form>
                <div class="drug_detail_btn">                   
                    <button style="background: #3498db" onclick="editDrug()">Edit</button>
                    <button onclick="backToDrug()" style="background: #e74c3c">Cancel</button>
                </div>
            </div>
        </div>
        <script>
            function backToDrug() {
                window.history.go(-1);
                return false;
            }
            async function editDrug() {
                const registedCode = document.getElementById("registedCode").value;
                const drugName = document.getElementById("drugname").value;
                const company = document.getElementById("company").value;
                const origin = document.getElementById("origin").value;
                const price = document.getElementById("price").value;
                const unit = document.getElementById("unit").value;

//                console.log(unit);
            }
        </script>
    </body>
</html>
