<%-- 
    Document   : pres2
    Created on : Apr 15, 2022, 9:36:35 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <link href="<c:url value = "/css/prescriptions.css" />" rel="stylesheet" />
    </head>
    <body>
        <div style="width: 100%; height: 100vh; display: flex;; align-items: center; justify-content: center;">
            <div class="pre_right_left" style="width: 40%; height: 90%; padding: 1%; background: #FFCCCC;">
                <div class="form-group">
                    <label for="email" style="font-weight: 600">Doctor name</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email" style="font-weight: 600">Patient name</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email" style="font-weight: 600">Diagnose</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email" style="font-weight: 600">Manifestation</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group" >
                    <label for="email" style="font-weight: 600">Note</label>
                    <input type="text" class="form-control">
                </div>    
                <div class="form-group" >
                    <label for="email" style="font-weight: 600">Prescript date</label>
                    <input type="text" class="form-control">
                </div>   
                
                <div class="pre_right_left_btn" style="margin-top: 10%">                
                    <button style="background: #2ecc71">Add</button>
                    <button style="background: #e74c3c">Cancel</button>
                </div>
            </div>
        </div>
    </body>
</html>
