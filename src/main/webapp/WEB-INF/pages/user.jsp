<%-- Document : register Created on : Apr 5, 2022, 8:37:26 AM Author : Admin
--%> <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
      crossorigin="anonymous"
    />
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
      integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
      integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
      crossorigin="anonymous"
    ></script>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <%@ taglib
    prefix="form" uri="http://www.springframework.org/tags/form" %> <%@ taglib
    prefix="spring" uri="http://www.springframework.org/tags" %>

    <style>
      #display_image {
        width: 400px;
        height: 225px;
        border: 1px solid black;
        background-position: center;
        background-size: cover;
        z-index: 10;
      }
    </style>
  </head>
  <body>
    <div
      class="container"
      style="
        max-width: 700px;
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
      "
    >
      <div
        style="
          width: 60%;
          height: 95%;
          background: #003079;
          border-radius: 20px;
        "
      >
        <p style="text-align: center; font-size: 30px; color: white">
          Register
        </p>
        <form:form
          class="container"
          style="height: 80%; width: 100%; padding: 0 40px"
          method="post"
          action="/PrivateClinicProjekt/home/test"
          modelAttribute="user"
        >
          <div class="form-group" style="padding-bottom: 10px; color: white">
            <label for="email">Username:</label>
            <form:input
              type="email"
              class="form-control"
              placeholder="Username"
              path="username"
            />
          </div>
          <div class="form-group" style="padding-bottom: 10px; color: white">
            <label for="pwd">Password:</label>
            <form:password
              class="form-control"
              placeholder="Enter password"
              path="password"
            />
          </div>

          <label for="img"><b>Select Image:</b></label>
          <!-- <form:input type="file" path="avatar" id="img" name="img" accept="image/*"  /> -->

          <input type="submit" class="btn btn-primary" style="width: 100%" />
        </form:form>
        <input type="file" id="image_input" accept="image/png, image/png" />

        <div id="display_image"></div>
      </div>
    </div>
    <script>
      const image_input = document.querySelector("#image_input");
      var uploaded_image;

      image_input.addEventListener("change", function () {
        console.log(
          "🚀 ~ file: user.jsp ~ line 66 ~ image_input.addEventListener ~ image_input",
          image_input.value
        );
        const reader = new FileReader();
        reader.addEventListener("load", () => {
          uploaded_image = reader.result;
          document.querySelector(
            "#display_image"
          ).style.backgroundImage = `url(${uploaded_image})`;
        });
        reader.readAsDataURL(this.files[0]);
      });
    </script>
  </body>
</html>
