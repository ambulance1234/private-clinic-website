<%-- 
    Document   : edit
    Created on : Apr 9, 2022, 8:52:55 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    </head>
    <body>
        <div style="width: 100%; height: 80vh;">
            <!-- Header  -->
            <div class="container-fluid">
                <div class="row" style="background: #003079; color: white; font-size: 20px" >
                    <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center">Milos Clinic</div>
                    <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center" >
                        <img src="https://freetuts.net/upload/product_series/images/2021/11/29/2446/ricardo-milos.JPG" style="width: 80px; height: 80px; border-radius: 50%;"/>
                    </div>
                    <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center">
                        Dr.Ricardo
                        <img src="https://freetuts.net/upload/product_series/images/2021/11/29/2446/ricardo-milos.JPG" style="width: 70px; height: 70px; border-radius: 50%; margin-left: 2%"/>
                    </div>
                </div>
            </div>
            <!-- 4 tags bên trái -->
            <div>
                <div class="d-flex justify-content-start ">
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FF5858">Patient</button>
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FFBC58">Drug</button>
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FCFF58">Nurse</button>
                    <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #B6FF58">Doctor</button>
                </div>
            </div>
            <!-- body -->
            <div class="container-fluid" style="height: 80%;">
                <div style="width: 100%; display: flex; justify-content: center; align-items: center">
                    <div class="col-sm-6" style=" padding:0">
                        <div style="height: 200px; ">

                            <div style="display: flex; justify-content: space-around; ">
                                <div class="form-group" style="width: 60px">
                                    <label for="usr">ID</label>
                                    <input type="text" disabled class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 170px">
                                    <label for="usr">First Name</label>
                                    <input type="text" disabled class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 170px">
                                    <label for="usr">Last Name</label>
                                    <input type="text" disabled class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 120px">
                                    <label for="usr">Tag</label>
                                    <input type="text" disabled class="form-control" style="background: #DFDFDF">
                                </div>                           
                                <div class="form-group" style="width: 60px">
                                    <label for="usr">Age</label>
                                    <input type="text" disabled class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 60px">
                                    <label for="usr">Sex</label>
                                    <input type="text" disabled class="form-control" style="background: #DFDFDF">
                                </div>
                            </div>
                            <div style="display: flex; justify-content: space-around; width: 100%;">
                                <div class="form-group" style="width: 50%">
                                    <label for="comment">Email</label>
                                    <textarea class="form-control" style="background: #DFDFDF; resize: none" rows="1"></textarea>
                                    <label for="usr">Phone</label>
                                    <input type="text" class="form-control" style="background: #DFDFDF">
                                </div>
                                <div class="form-group" style="width: 45%">
                                    <label for="usr">Created at</label>
                                    <input type="text" class="form-control" style="background: #DFDFDF">
                                    <div style="height: 50%; align-items: end; " class="d-flex flex-row-reverse">
                                        <button class="btn" style="background: #00BA61; color: white; width: 40%">Save</button>
                                        <button class="btn" style="background: #970000; color: white; width: 40%; margin-right: 10% ">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="height: auto; display: flex; align-items: center; justify-content: center; flex-direction: column">
                            <!--                          <button>
                                                          <img style="width: 300px; height: 300px" src="https://pbs.twimg.com/profile_images/1243806677608083456/dgTUsgBf_400x400.jpg" />
                                                      </button>                 -->
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
