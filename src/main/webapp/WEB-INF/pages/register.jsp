<%-- 
    Document   : register
    Created on : Apr 5, 2022, 8:37:26 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>   
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <link href="<c:url value = "/css/register.css" />" rel="stylesheet" />
    </head>
    <body>
        <div class="container" style="max-width: 1000px; height: 100vh; display: flex; justify-content: center; align-items: center;">
            <div style="width: 50%; height: auto; background: #003079; border-radius: 20px; margin: 1em 0">
                <p style="text-align: center; font-size: 30px; color: white">Register</p>
                <c:if test="${errMsg != null}" >
                    <div class="alert alert-danger">
                        ${errMsg}
                    </div>
                </c:if>
                <form:form class="container" style="display:flex; justify-content:space-between; flex-direction:column"  method="post" action="/PrivateClinicProjekt/home/register" modelAttribute="accounts" enctype="multipart/form-data" >
                    <%--F<form:errors path="*" element="div" cssClass="alert alert-danger" />--%>
                    <div class="container-fluid" style="height: 80%; width: 100%; padding: 0 40px">
                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="email">Email:</label>
                            <form:input path="email" type="email" class="form-control" placeholder="Enter email" />
                            <form:errors path="email" cssClass="text-danger" />
                        </div>

                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">Password:</label>
                            <form:password path="password" class="form-control" placeholder="Enter password" />
                            <form:errors path="password" cssClass="text-danger" />
                        </div>

                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">Confirm Password:</label>
                            <form:password path="confirmPassword" class="form-control" placeholder="Confirm password" />
                        </div>

                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">Phone:</label>
                            <form:input path="phone" type="text" class="form-control" placeholder="Phone number" />
                        </div>

                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">First Name:</label>
                            <form:input path="firstName" type="text" class="form-control" placeholder="Your first name" />
                        </div>

                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">Last Name:</label>
                            <form:input path="lastName" type="text" class="form-control" placeholder="Your last name" />
                        </div>

                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">Age:</label>
                            <form:input path="age"  type="text" class="form-control" placeholder="Your age" />
                        </div>

                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">Gender:</label>
                            <form:input path="genderId.genderName" type="text" class="form-control" placeholder="Gender" />
                        </div>
                        
                        <div class="form-group" style="padding-bottom: 10px; color: white">
                            <label for="pwd">Please choose image:</label>
                            <form:input path="avatar" type="file" id="myFile" name="filename" accept="image/*" class="regist_chooseimg" /> 
                        </div>

                        <input value="Register" type="submit" class="btn btn-primary" style="width: 100%; font-size: 20px"/>

                    </div>
                        
                                                                  



                </div>
<!--                <div style="width: 50%; height: 95%; background: #003079; border-radius: 20px; padding: 20px">
                    <div style="width: 100%; height: 100%">
                        


                    </div>
                    

                </div>-->

            </form:form>
        </div>
    </body>
</html>
