<%-- 
    Document   : header
    Created on : Apr 9, 2022, 9:50:06 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Header  -->
<div class="container-fluid">
    <div class="row" style="background: #003079; color: white; font-size: 20px" >
        <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center">Milos Clinic</div>

        <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center" >
            <img src="https://freetuts.net/upload/product_series/images/2021/11/29/2446/ricardo-milos.JPG" style="width: 80px; height: 80px; border-radius: 50%;"/>
        </div>
        <c:if test="${pageContext.request.userPrincipal.name == null}">
            <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center" >
                <img src="https://cdn.discordapp.com/attachments/880695496811884554/964485854032715776/ricardo-doctor.jpg" style="width: 80px; height: 80px; border-radius: 50%;"/>
            </div>
            <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center">
                <a href="<c:url value="/home/login" />">
                    Login  
                </a>
                <img src="https://cdn.discordapp.com/attachments/880695496811884554/964485854032715776/ricardo-doctor.jpg" style="width: 70px; height: 70px; border-radius: 50%; margin-left: 2%"/>
            </div>
        </c:if>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center" >
                <img src="https://cdn.discordapp.com/attachments/880695496811884554/964485854032715776/ricardo-doctor.jpg" style="width: 80px; height: 80px; border-radius: 50%;"/>
            </div>
            <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center" >
                <a href="<c:url value="/logout" />">
                    Logout
                </a>
            </div>
            <div class="col-sm-4" style="display: flex; justify-content: center; align-items: center; font-size: 16px; border-radius: 50%">
                ${pageContext.request.userPrincipal.name}
                <img src="${pageContext.session.getAttribute("currentUser").avatar}" style="width: 50px; height: 50px; border-radius: 50%; margin-left: 2%"/>
            </div>


        </c:if>

    </div>
</div>
<!-- 4 tags bên trái -->
<div>
    <div class="d-flex justify-content-start ">
        <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FF5858">
            <a href="<c:url value="/home/patient" />">
                Patient  
            </a>
        </button>
        <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FFBC58">
            <a href="<c:url value="/home/drug" />">
                Drug  
            </a>
        </button>
        <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FCFF58">
            <a href="<c:url value="/home/account/tag=nurse"/>">
                Nurse  
            </a>
        </button>
        <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #B6FF58">
            <a href="<c:url value="/home/account/tag=doctor" />">
                Doctor  
            </a>
        </button>
        <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #FFBC58">
            <a href="<c:url value="/home/account/tag=doctor" />">
                Admin  
            </a>
        </button>
        <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #B6FF58">
            <a href="<c:url value="/admin/report" />">
                Report  
            </a>
        </button>
        <button class="p-2" style="margin:5px; border-radius: 5px; width: 100px; background: #B6FF58">
            <a href="<c:url value="/home/frequency" />">
                Frequency  
            </a>
        </button>
    </div>
</div>

