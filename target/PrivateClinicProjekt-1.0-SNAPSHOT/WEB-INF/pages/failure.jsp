<%-- 
    Document   : failure
    Created on : Apr 10, 2022, 6:25:54 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Failure</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <div class="flex-column" style="width: 100%; height: 100vh; display: flex; justify-content: center; align-items: center;">
            <div style="display: flex; justify-content: space-between; align-items: center; height: 100px; font-size: 30px; color: red">
                <span class="material-icons" style="font-size: 30px">
                    highlight_off
                </span>
                <div>Failure</div>
            </div>
            <button type="button" class="btn btn-danger" style="width: 200px" >Go back</button>
        </div>
    </body>
</html>
