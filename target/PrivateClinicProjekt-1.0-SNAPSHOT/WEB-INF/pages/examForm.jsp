<%-- 
    Document   : examForm
    Created on : Apr 10, 2022, 1:55:33 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="<c:url value = "/css/examForm.css" />" rel="stylesheet" />

<div class="examForm">
    <div class="examTemp">
        <p class="exam_title">Create new examination</p>
        <div class="dropdown">
            <span class="sizeWeight">Doctor name:</span>
            <button class="btn btn-sm btn-secondary dropdown-toggle" style="background-color: white; color: black;" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Doctor Name
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdown_user">
                <form class="px-4 py-2">
                    <input type="search" class="form-control search" placeholder="Search..." autofocus="autofocus">
                </form>
                <div class="menuItems">
                    <c:forEach items="${doctors}" var="d">
                        <input type="button" class="dropdown-item" type="button" value="${d.accountId}-${d.phone}-${d.firstName} ${d.lastName}" />
                    </c:forEach>
                </div>
                <div style="display:none;" class="dropdown-header dropdown_empty">No entry found</div>
            </div>
        </div>
        <!-- Dưới này là dropdown search patient này -->
        <div class="dropdown">
            <span class="sizeWeight">Patient name:</span>
            <button class="btn btn-sm btn-secondary dropdown-toggle" style="background-color: white; color: black;" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Patient Name
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdown_user">
                <form class="px-4 py-2">
                    <input type="search" class="form-control search" placeholder="Search..." autofocus="autofocus">
                </form>
                <div class="menuItems">
                    <c:forEach items="${patients}" var="p">
                        <input type="button" class="dropdown-item" type="button" value="${p.patientId}-${p.nationalId}-${p.firstName} ${p.lastName}" />
                    </c:forEach>
                </div>
                <div style="display:none;" class="dropdown-header dropdown_empty">No entry found</div>
            </div>
        </div>
        <!-- Dưới này là chọn ngày này -->
        <form action="/action_page.php">
            <label for="birthdaytime" class="sizeWeight">Choose date:</label>
            <input type="date" id="birthdaydate"  name="birthdaytime">
            <!--<input type="submit">-->
        </form>
        <!-- Dưới này là chọn giờ này -->
        <form action="/action_page.php">
            <label for="birthdaytime" class="sizeWeight">Choose time:</label>
            <input type="time" id="birthdaytime" value="00:00:01"  name="birthdaytime">
            <!--<input type="submit">-->
        </form>
        <div class="form-group">
            <label class="sizeWeight">Reason:</label>
            <input type="text" class="form-control">
        </div>
        <div class="exambtn_gr">
            <button style="background:#00b894 " onclick="log()">Create</button>
            <button style="background:#d63031 ">Cancel</button>
        </div>

    </div>
</div>



<script>
    $('.dropdown').each(function (index, dropdown) {
        //Find the input search box
        let search = $(dropdown).find('.search');

        //Find every item inside the dropdown
        let items = $(dropdown).find('.dropdown-item');

        //Capture the event when user types into the search box
        $(search).on('input', function () {
            filter($(search).val().trim().toLowerCase())
        });

        //For every word entered by the user, check if the symbol starts with that word
        //If it does show the symbol, else hide it
        function filter(word) {
            let length = items.length
            let collection = []
            let hidden = 0
            for (let i = 0; i < length; i++) {
                if (items[i].value.toString().toLowerCase().includes(word)) {
                    $(items[i]).show()
                } else {
                    $(items[i]).hide()
                    hidden++
                }
            }

            //If all items are hidden, show the empty view
            if (hidden === length) {
                $(dropdown).find('.dropdown_empty').show();
            } else {
                $(dropdown).find('.dropdown_empty').hide();
            }
        }

        //If the user clicks on any item, set the title of the button as the text of the item
        $(dropdown).find('.dropdown-menu').find('.menuItems').on('click', '.dropdown-item', function () {
            $(dropdown).find('.dropdown-toggle').text($(this)[0].value);
            $(dropdown).find('.dropdown-toggle').dropdown('toggle');
        })
    });
    function log() {
        const time = document.getElementById('birthdaytime').value;
        const date = document.getElementById('birthdaydate').value;
        console.log(date, time);
    }
</script>
