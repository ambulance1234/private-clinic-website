<%-- 
    Document   : prescriptions
    Created on : Apr 8, 2022, 9:31:47 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link href="<c:url value = "/css/fixedTable.css" />" rel="stylesheet" />
<link href="<c:url value = "/css/prescriptions.css" />" rel="stylesheet" />
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- body -->
<div class="pre_body">
    <div class="pre_left">
        <div class="pre_left_top">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Doctor</th>
                        <th>Email</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>John</td>
                        <td>Doe</td>
                        <td>john@example.com</td>
                    </tr>

                </tbody>
            </table>
        </div>
        <div class="pre_left_bot">
            <!--            <div class="input-group mb-3 pre_search_bar">
                            <input type="text" class="form-control" placeholder="Search drug">
                            <div class="input-group-append">
                                <button class="btn" type="submit" style="background: #0096C6; color: white;">Search</button>
                            </div>
                        </div>-->
            <div class="pre_left_bot_druginfo" >
                <div class="druginfo_tag" style="width: 350px;">
                    Drug's name
                    <div class="dropdown" style="width: 100%; height: auto">
                        <button style="width: 100%; background: white; color: black;" class="btn btn-sm btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Find drug
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdown_user">
                            <form class="px-4 py-2">
                                <input type="search" class="form-control search" placeholder="Suche.." autofocus="autofocus">
                            </form>
                            <div class="menuItems">
                                <input type="button" class="dropdown-item" type="button" value="Test1dasadas asdas asdasd asdas asd" />
                                <input type="button" class="dropdown-item" type="button" value="Test2" />
                                <input type="button" class="dropdown-item" type="button" value="Test3" />
                            </div>
                            <div style="display:none;" class="dropdown-header dropdown_empty">No entry found</div>
                        </div>
                    </div>
                </div>
                <div class="druginfo_tag" style="width: 120px">
                    Unit
                    <input value="Viên"/>
                </div>
                <div class="druginfo_tag" style="width: 80px">
                    Amount
                    <input value="1"/>
                </div>
                <div class="druginfo_tag" style="width: 80px">
                    Quantity
                    <input value="2"/>
                </div>
                <div class="druginfo_tag" style="width: 300px">
                    Use in part
                    <span> 

                        <select id="select1" style="width: 100%">

                            <c:forEach items="${parts}" var="part">
                                <option value="${part.partId}-${part.partName}">${part.partId}-${part.partName}</option>    
                            </c:forEach>
                        </select>
                    </span>                            
                </div>
                <div class="druginfo_btn" >
                    <input type="submit" class="btn" style="background: black; color: white" value="Submit" />
                </div>
            </div>         
        </div>
    </div>
    <div class="pre_right">
        <div class="pre_right_left" style="background: #FFCCCC;">
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Doctor name</label>
                <input type="text" disabled class="form-control">
            </div>
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Patient name</label>
                <input type="text" disabled class="form-control">
            </div>
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Diagnose</label>
                <input type="text" disabled class="form-control">
            </div>
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Manifestation</label>
                <input type="text" disabled class="form-control">
            </div>
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Note</label>
                <input type="text" disabled class="form-control">
            </div>    
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Prescript date</label>
                <input type="text" disabled class="form-control">
            </div>   
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Created at</label>
                <input type="text" disabled class="form-control">
            </div>
            <div class="form-group" style="margin-bottom: 0;">
                <label for="email" style="font-weight: 600; margin-bottom: 0;">Updated at</label>
                <input type="text" disabled class="form-control">
            </div>
            <div class="pre_right_left_btn">
                <button style="background: #3498db">Edit</button>
                <button style="background: #2ecc71">Add</button>
                <button style="background: #e74c3c">Delete</button>
            </div>
        </div>
        <div class="pre_right_right" style="background: #FFE973">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Drug Name</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>Use day</th>
                        <th>Qty use</th>
                        <th>Part</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>20% Fat Emulsion Injection Soybean oil</td>
                        <td>100.000</td>
                        <td>3</td>
                        <td>bottle - chai</td>
                        <td>1</td>
                        <td>2</td>
                        <td>morning - afternoon</td>
                        <td><span class="material-icons">delete_outline</span></td>
                    </tr>
                    <tr>
                        <td>20% Fat Emulsion Injection Soybean oil</td>
                        <td>100.000</td>
                        <td>3</td>
                        <td>bottle - chai</td>
                        <td>1</td>
                        <td>2</td>
                        <td>morning - afternoon</td>
                        <td><span class="material-icons">delete_outline</span></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $('.dropdown').each(function (index, dropdown) {
        //Find the input search box
        let search = $(dropdown).find('.search');

        //Find every item inside the dropdown
        let items = $(dropdown).find('.dropdown-item');

        //Capture the event when user types into the search box
        $(search).on('input', function () {
            filter($(search).val().trim().toLowerCase())
        });

        //For every word entered by the user, check if the symbol starts with that word
        //If it does show the symbol, else hide it
        function filter(word) {
            let length = items.length
            let collection = []
            let hidden = 0
            for (let i = 0; i < length; i++) {
                if (items[i].value.toString().toLowerCase().includes(word)) {
                    $(items[i]).show()
                } else {
                    $(items[i]).hide()
                    hidden++
                }
            }

            //If all items are hidden, show the empty view
            if (hidden === length) {
                $(dropdown).find('.dropdown_empty').show();
            } else {
                $(dropdown).find('.dropdown_empty').hide();
            }
        }

        //If the user clicks on any item, set the title of the button as the text of the item
        $(dropdown).find('.dropdown-menu').find('.menuItems').on('click', '.dropdown-item', function () {
            $(dropdown).find('.dropdown-toggle').text($(this)[0].value);
            $(dropdown).find('.dropdown-toggle').dropdown('toggle');
        })
    });
</script>
