<%-- Document : login Created on : Apr 6, 2022, 10:59:00 AM Author : Admin --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
            crossorigin="anonymous"
            />
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
            integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
            integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
            crossorigin="anonymous"
        ></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

                <title>Login Page</title>
            </head>
            <body>
                <div
                    class="container"
                    style="
                    max-width: 700px;
                    height: 100vh;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    flex-direction: column;
                    "
                    >
                    <div
                        style="
                        width: 60%;
                        height: 90%;
                        background: #003079;
                        border-radius: 20px;
                        "
                        >
                        <div
                            style="
                            width: 100%;
                            height: 50%;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            "
                            >
                            <img
                                src="https://cdn.discordapp.com/attachments/880695496811884554/964485854032715776/ricardo-doctor.jpg"
                                alt="Cinque Terre"
                                style="
                                width: 70%;
                                height: 80%;
                                border-radius: 50%;
                                object-fit: cover;
                                "
                                />
                        </div>
                        <form
                            class="container"
                            style="height: 50%; width: 100%; padding: 0 40px"
                            method="post"
                            action="/PrivateClinicProjekt/home/login"
                            >
                            <!--  -->
                            <div class="form-group" style="padding-top: 20px; color: white">
                                <label for="email">Email:</label>
                                <input
                                    type="email"
                                    name="email"
                                    class="form-control"
                                    placeholder="Enter email"
                                    />
                            </div>
                            <div class="form-group" style="padding: 20px 0; color: white">
                                <label for="pwd">Password:</label>
                                <input
                                    type="password"                     
                                    name="password"
                                    class="form-control"
                                    placeholder="Enter password"
                                    />
                            </div>

                            <input
                                type="submit"
                                value="Login"
                                class="btn btn-primary"
                                style="width: 100%"
                                />
                        </form>
                    </div>
                </div>
            </body>
        </html>
