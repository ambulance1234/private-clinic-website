<%-- Document : report Created on : Apr 7, 2022, 8:56:57 PM Author : Admin --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div
  class="container-fluid"
  style="
    display: flex;
    justify-content: space-between;
    padding: 0 3em;
    flex-wrap: wrap;
  "
>
  <div style="width: 60%">
    <canvas id="myChart"></canvas>
  </div>
  <table style="width: 30%" class="table table-hover">
    <thead style="background: #ffb8b8; border-radius: 15px 15px 0 0 !important">
      <tr style="text-align: center">
        <th>Time</th>
        <th>Value</th>
      </tr>
    </thead>
    <tbody style="text-align: center">
      <tr style="height: 40px">
        <td>John</td>
        <td>Doe</td>
      </tr>
      <tr style="height: 40px">
        <td>Mary</td>
        <td>Moe</td>
      </tr>
      <tr style="height: 40px">
        <td>Mary</td>
        <td>Moe</td>
      </tr>
      <tr style="height: 40px">
        <td>Mary</td>
        <td>Moe</td>
      </tr>
      <tr style="height: 40px">
        <td>Mary</td>
        <td>Moe</td>
      </tr>
      <tr style="height: 40px">
        <td>Mary</td>
        <td>Moe</td>
      </tr>
    </tbody>
  </table>

  <form onsubmit="handleSubmit()">
    <div class="mt-5 d-flex justify-content-between" style="width: 550px">
      <!--                    <input type="text" class="form-control text-center " placeholder="Month" style="width: 150px">
                                        <input type="text" class="form-control text-center " placeholder="Quarter" style="width: 150px">
                                        <input type="text" class="form-control text-center " placeholder="Year" style="width: 150px">-->

      <!--                    <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Dropdown button
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">Link 1</a>
                                                <a class="dropdown-item" href="#">Link 2</a>
                                                <a class="dropdown-item-text" href="#">Text Link</a>
                                                <span class="dropdown-item-text">Just Text</span>
                                            </div>
                                        </div>-->

      <select
        class="custom-select"
        id="select1"
        style="width: 150px; text-align: center"
      >
        <option>Month</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
      </select>

      <select
        class="custom-select"
        id="select1"
        style="width: 150px; text-align: center"
      >
        <option>Quarter</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
      </select>

      <select
        class="custom-select"
        id="select1"
        style="width: 150px; text-align: center"
      >
        <option>Year</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option>
        <option value="2022">2022</option>
        <option value="2023">2023</option>
        <option value="2024">2024</option>
        <option value="2025">2025</option>
      </select>

      <button onclick="getOption()" type="submit" class="btn btn-success">
        Filter
      </button>
    </div>
  </form>
</div>

<script>
  function handleSubmit() {
    return false;
  }

  function getOption() {
    selectElement = document.querySelector("#select1");
    output = selectElement.value;
    document.querySelector(".output").textContent = output;
  }

  const labels = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const data = {
    labels: labels,
    datasets: [
      {
        label: "Database",
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgb(255, 99, 132)",
        data: [0, 10, 5, 2, 20, 30, 45, 55, 60, 65],
      },
    ],
  };

  const config = {
    type: "line",
    data: data,
    options: {},
  };

  const myChart = new Chart(document.getElementById("myChart"), config);
</script>
