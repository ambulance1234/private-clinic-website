<%-- Document : drug Created on : Apr 6, 2022, 2:53:32 PM Author : Admin --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="<c:url value = "/css/drug_detail.css" />" rel="stylesheet" />
        <link href="<c:url value = "/css/fixedTable.css" />" rel="stylesheet" />
    
        <div style="width: 100%; height: 80vh;">
            <!-- Header  -->
            
            <!-- 4 tags bên trái -->
           
            <!-- body -->
            <div class="container-fluid" style="height: 100%;">
                <div class="row" style="height: 100%; ">
                    <div class="col-sm-6" style="height: 100%">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Search Patient...">
                            <div class="input-group-append">
                                <button class="btn btn-primary" style="color: white" type="submit">Search</button>  
                            </div>
                        </div>
                        <div class="drug_table fixTableHead">
                            <table class="table table-hover" style="color: white; text-align: center;">
                                <thead style="background: #0804AB;">
                                    <tr>
                                        <th>Registed Code</th>
                                        <th>Drug Name</th>
                                        <th>Origin</th>
                                        <th>Price</th>
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody style="background: #585798">
                                    <c:forEach items="${drugs}" var="d"> 
                                        <tr>
                                            <c:url 
                                                value="/home/drug/detail/${d.drugId}" 
                                                var="detail" 
                                                />
                                            <td>${d.drugId}</td>
                                            <td>${d.drugName}</td>
                                            <td>${d.origin}</td>
                                            <td>${d.price}</td>    
                                            <td>
                                                <a href="${detail}" style="color: white">
                                                    <span class="material-icons">
                                                        info
                                                    </span>
                                                </a>
                                            </td>
                                        </tr>                                     
                                    </c:forEach>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6 drug_detail">
                        <p2 class="drug_detail_title">Drug Detail</p2>
                        <form class="drug_detail_form">
                            <label>Drug name</label>
                            <input disabled id="drugname" value="${drug.drugName}"/>    
                        </form>
                        <form class="drug_detail_form">
                            <label>Company</label>
                            <input disabled id="company" value="${drug.company}"/>
                        </form>
                        <form class="drug_detail_form">
                            <label>Origin</label>
                            <input disabled id="origin" value="${drug.origin}"/>
                        </form>
                        <form class="drug_detail_form">
                            <label>Price</label>
                            <input disabled id="price" value="${drug.price * 1000} VND "/>
                        </form>
                        <form class="drug_detail_form">
                            <label>Unit</label>
                            <input disabled id="unit" value="${drug.unit.unitName}"/>
                        </form>
                        <div class="drug_detail_btn">
                            <button onclick="openEditDrug(${drug.drugId})" style="background: #3498db">Edit</button>
                            <button onclick="openAddDrug()" style="background: #2ecc71">Add</button>
                            <button style="background: #e74c3c">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
        <script>
            function openAddDrug(){
                location.href = "./addDrug";
            }
            function openEditDrug(id){
                location.href = `http://localhost:9090/PrivateClinicProjekt/home/editDrug?id=${id}`;
            }
        </script>
      
