<%-- Document : frequency Created on : Apr 7, 2022, 11:48:15 PM Author : Admin
--%> <%@page contentType="text/html" pageEncoding="UTF-8"%>

<div
  class="container-fluid"
  style="
    display: flex;
    justify-content: space-between;
    padding: 0 3em;
    flex-wrap: wrap;
  "
>
  <div style="width: 60%">
    <canvas id="myChart"></canvas>

    <form>
      <div class="mt-5 d-flex justify-content-between" style="width: 550px">
        <!-- <input type="text" class="form-control text-center " placeholder="Month" style="width: 150px">
                        <input type="text" class="form-control text-center " placeholder="Quarter" style="width: 150px">
                        <input type="text" class="form-control text-center " placeholder="Year" style="width: 150px"> -->

        <select
          class="custom-select"
          id="select1"
          style="width: 150px; text-align: center"
        >
          <option>Month</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
        </select>

        <select
          class="custom-select"
          id="select1"
          style="width: 150px; text-align: center"
        >
          <option>Quarter</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
        </select>

        <select
          class="custom-select"
          id="select1"
          style="width: 150px; text-align: center"
        >
          <option>Year</option>
          <option value="2015">2015</option>
          <option value="2016">2016</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
          <option value="2023">2023</option>
          <option value="2024">2024</option>
          <option value="2025">2025</option>
        </select>

        <button type="button" class="btn btn-success">Filter</button>
      </div>
    </form>
  </div>
  <div
    style="width: 30%"
    class="container d-flex justify-content-between flex-column"
  >
    <table class="table table-borderless rounded-circle">
      <thead
        style="background: #ffb8b8; border-radius: 15px 15px 0 0 !important"
      >
        <tr style="text-align: center">
          <th>Time</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody style="text-align: center">
        <tr style="height: 40px; background: #ffecec">
          <td>John</td>
          <td>Doe</td>
        </tr>
        <tr style="height: 40px; background: #ffecec">
          <td>Mary</td>
          <td>Moe</td>
        </tr>
        <tr style="height: 40px; background: #ffecec">
          <td>Mary</td>
          <td>Moe</td>
        </tr>
        <tr style="height: 40px; background: #ffecec">
          <td>Mary</td>
          <td>Moe</td>
        </tr>
        <tr style="height: 40px; background: #ffecec">
          <td>Mary</td>
          <td>Moe</td>
        </tr>
        <tr style="height: 40px; background: #ffecec">
          <td>Mary</td>
          <td>Moe</td>
        </tr>
      </tbody>
    </table>

    <div class="container mt-5">
      <form class="input-group mb-3">
        <input
          type="text"
          class="form-control"
          placeholder="Username"
          aria-label="Username"
          aria-describedby="basic-addon1"
        />
        <div class="input-group-prepend">
          <!--<span class="input-group-text" id="basic-addon1">@</span>-->
          <button type="submit" class="input-group-text">
            <i class="fas fa-search" id="basic-addon1"></i>
          </button>
        </div>
      </form>

      <table style="border-radius: 15px" class="table table-borderless">
        <thead style="background: #0065c1">
          <tr style="text-align: center">
            <th>ID</th>
            <th>Name</th>
            <th>Age</th>
          </tr>
        </thead>
        <tbody style="text-align: center">
          <tr style="height: 40px; background: #6db9ff">
            <td>John</td>
            <td>Doe</td>
            <td>Doe</td>
          </tr>
          <tr style="height: 40px; background: #6db9ff">
            <td>Mary</td>
            <td>Moe</td>
            <td>Doe</td>
          </tr>
          <tr style="height: 40px; background: #6db9ff">
            <td>Mary</td>
            <td>Moe</td>
            <td>Moe</td>
          </tr>
          <tr style="height: 40px; background: #6db9ff">
            <td>Mary</td>
            <td>Moe</td>
            <td>Doe</td>
          </tr>
          <tr style="height: 40px; background: #6db9ff">
            <td>Mary</td>
            <td>Moe</td>
            <td>Doe</td>
          </tr>
          <tr style="height: 40px; background: #6db9ff">
            <td>Mary</td>
            <td>Moe</td>
            <td>Doe</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
  const labels = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const data = {
    labels: labels,
    datasets: [
      {
        label: "Database",
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgb(255, 99, 132)",
        data: [0, 10, 5, 2, 20, 30, 45, 55, 60, 65, 100],
      },
    ],
  };

  const config = {
    type: "line",
    data: data,
    options: {},
  };

  const myChart = new Chart(document.getElementById("myChart"), config);
</script>
