package com.milosclinic.models;

import com.milosclinic.models.DetailDrugsPrescriptionsPK;
import com.milosclinic.models.Drugs;
import com.milosclinic.models.PartsOfDay;
import com.milosclinic.models.Prescriptions;
import com.milosclinic.models.Units;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

<<<<<<< HEAD
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:45:16", comments="EclipseLink-2.7.9.v20210604-rNA")
=======
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:57:32", comments="EclipseLink-2.7.9.v20210604-rNA")
>>>>>>> a42dcc279d976fe1e5db4e950068d32251bf9a93
@StaticMetamodel(DetailDrugsPrescriptions.class)
public class DetailDrugsPrescriptions_ { 

    public static volatile SingularAttribute<DetailDrugsPrescriptions, Integer> quantityPerUse;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, Units> unit;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, String> quantity;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, Double> price;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, PartsOfDay> useInPart;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, Drugs> drugId;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, DetailDrugsPrescriptionsPK> detailDrugsPrescriptionsPK;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, Integer> usePerDay;
    public static volatile SingularAttribute<DetailDrugsPrescriptions, Prescriptions> prescriptions;

}