package com.milosclinic.models;

import com.milosclinic.models.Genders;
import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.Prescriptions;
import com.milosclinic.models.Roles;
import com.milosclinic.models.Tags;
import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

<<<<<<< HEAD
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:45:16", comments="EclipseLink-2.7.9.v20210604-rNA")
=======
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:57:32", comments="EclipseLink-2.7.9.v20210604-rNA")
>>>>>>> a42dcc279d976fe1e5db4e950068d32251bf9a93
@StaticMetamodel(Accounts.class)
public class Accounts_ { 

    public static volatile SingularAttribute<Accounts, String> lastName;
    public static volatile SingularAttribute<Accounts, Boolean> onActive;
    public static volatile SingularAttribute<Accounts, Tags> tagId;
    public static volatile SingularAttribute<Accounts, Roles> roles;
    public static volatile SingularAttribute<Accounts, Genders> genderId;
    public static volatile SingularAttribute<Accounts, Integer> accountId;
    public static volatile SingularAttribute<Accounts, String> firstName;
    public static volatile SingularAttribute<Accounts, Date> createdAt;
    public static volatile SingularAttribute<Accounts, String> password;
    public static volatile SingularAttribute<Accounts, String> imageSource;
    public static volatile SingularAttribute<Accounts, String> phone;
    public static volatile ListAttribute<Accounts, MedicalExaminations> medicalExaminationsList;
    public static volatile ListAttribute<Accounts, Prescriptions> prescriptionsList;
    public static volatile SingularAttribute<Accounts, String> email;
    public static volatile SingularAttribute<Accounts, Integer> age;
    public static volatile SingularAttribute<Accounts, Date> updatedAt;

}