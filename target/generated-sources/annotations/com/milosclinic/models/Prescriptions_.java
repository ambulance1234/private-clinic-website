package com.milosclinic.models;

import com.milosclinic.models.Accounts;
import com.milosclinic.models.DetailDrugsPrescriptions;
import com.milosclinic.models.Patients;
import com.milosclinic.models.PrescriptionsPK;
import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

<<<<<<< HEAD
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:45:16", comments="EclipseLink-2.7.9.v20210604-rNA")
=======
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:57:32", comments="EclipseLink-2.7.9.v20210604-rNA")
>>>>>>> a42dcc279d976fe1e5db4e950068d32251bf9a93
@StaticMetamodel(Prescriptions.class)
public class Prescriptions_ { 

    public static volatile SingularAttribute<Prescriptions, String> note;
    public static volatile SingularAttribute<Prescriptions, Date> createdAt;
    public static volatile SingularAttribute<Prescriptions, String> manifestation;
    public static volatile SingularAttribute<Prescriptions, String> diagnose;
    public static volatile SingularAttribute<Prescriptions, Patients> patients;
    public static volatile SingularAttribute<Prescriptions, Accounts> accounts;
    public static volatile SingularAttribute<Prescriptions, DetailDrugsPrescriptions> detailDrugsPrescriptions;
    public static volatile SingularAttribute<Prescriptions, PrescriptionsPK> prescriptionsPK;
    public static volatile SingularAttribute<Prescriptions, Date> prescriptDate;
    public static volatile SingularAttribute<Prescriptions, Date> updatedAt;

}