package com.milosclinic.models;

import com.milosclinic.models.Genders;
import com.milosclinic.models.MedicalExaminations;
import com.milosclinic.models.Prescriptions;
import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

<<<<<<< HEAD
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:45:16", comments="EclipseLink-2.7.9.v20210604-rNA")
=======
@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-04-16T11:57:32", comments="EclipseLink-2.7.9.v20210604-rNA")
>>>>>>> a42dcc279d976fe1e5db4e950068d32251bf9a93
@StaticMetamodel(Patients.class)
public class Patients_ { 

    public static volatile SingularAttribute<Patients, String> lastName;
    public static volatile SingularAttribute<Patients, String> address;
    public static volatile SingularAttribute<Patients, Integer> patientId;
    public static volatile SingularAttribute<Patients, Genders> genderId;
    public static volatile SingularAttribute<Patients, Double> weight;
    public static volatile SingularAttribute<Patients, String> firstName;
    public static volatile SingularAttribute<Patients, Date> createdAt;
    public static volatile SingularAttribute<Patients, String> nationalId;
    public static volatile ListAttribute<Patients, MedicalExaminations> medicalExaminationsList;
    public static volatile ListAttribute<Patients, Prescriptions> prescriptionsList;
    public static volatile SingularAttribute<Patients, Integer> age;
    public static volatile SingularAttribute<Patients, Double> height;
    public static volatile SingularAttribute<Patients, Date> updatedAt;

}